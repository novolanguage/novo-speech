> See [./projects/novo-speech](./projects/novo-speech) for a README

## Publishing

Publish the package by running the following commands from within the `./projects/novo-speech` directory:

```bash
$ npm run build
$ npm test
$ cd dist
$ npm publish # note we're publishing ./dist!
```
