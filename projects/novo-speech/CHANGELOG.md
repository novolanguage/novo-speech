# Changelog

### [0.4.4](https://www.npmjs.com/package/@novo-learning/speech/v/0.4.4) - 25-11-2021
- Upgrade other peer dependencies
- Rename ng to ng1 for novo-cmn111 phoneset

### [0.4.3](https://www.npmjs.com/package/@novo-learning/speech/v/0.4.3) - 25-11-2021
- Upgrade peer-dependency of Angular < 13

### [0.4.2](https://www.npmjs.com/package/@novo-learning/speech/v/0.4.2) - 28-10-2021
- Remove uh and ng1 phonemes from novo-cmn111 phoneset

### [0.4.1](https://www.npmjs.com/package/@novo-learning/speech/v/0.4.1) - 08-06-2021
- Add Recorder.None recorder api. This allows to connect to the speech analyser without the intent to record (for instance only get pronunciations)
- Allow novo speech library to be called from nodejs scripts. Add the following two definitions to the top of the script for the library to work from a NodeJS script:
1. global.fetch = require('node-fetch');
2. global.WebSocket = require('ws');

### [0.4.0](https://www.npmjs.com/package/@novo-learning/speech/v/0.4.0) - 01-06-2021
- Enable phoneme sounds for novo-nl37
- Catch connection loss rejection while polling for connection quality. Connection quality is set to 'error'.

### [0.3.8](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.8) - 17-02-2021
- Do not restart a session when invalidated by client

### [0.3.7](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.7) - 08-02-2021
- When calling checkConnection on the SessionManager tries to reestablish the transport connection when it was lost
- Outstanding requests to the speech backend are rejected when the connection to the speech backend is lost.

### [0.3.6](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.6) - 13-01-2021
#### Bug fixes
- On iOS 14.3 an incorrect recorder was chosen resulting in an on going permission denied failure

### [0.3.5](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.5) - 11-01-2021
#### Bug fixes
- Named error when audio permissions are not granted in cordova ar enow all caps. Errors thrown are:
1. CORDOVA.MICROPHONE.PERMISSION.DENIED_ALWAYS (iOS and Android)
2. CORDOVA.MICROPHONE.PERMISSION.DENIED_ONCE (Android only: dismissed without saying dismiss always)
3. CORDOVA.MICROPHONE.PERMISSION.RESTRICTED (iOS only: permission denied and user cannot change permission, ie parent control)

### [0.3.4](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.4) - 07-01-2021
#### Bug fixes
- Ensure recorder is available when getting a grammar

### [0.3.3](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.3) - 05-01-2021
#### Bug fixes
- Throw a named error when audio permissions are not granted in cordova

### [0.3.2](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.2) - 01-12-2020
#### Bug fixes
- Do not throw an error when a request is cancelled on request
- Correct detection of already waiting for session

### [0.3.1](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.1) - 11-11-2020

#### Feature
- Add ability to force a new connetion check instead of reusing an existing one

### [0.3.0](https://www.npmjs.com/package/@novo-learning/speech/v/0.3.0) - 06-11-2020

#### Bug fixes
- Close mediastream when recording is terminated. This removes the recording icon on browsers tab
- Set codec when reconnecting to the speech recognizer in case of websocket failure/closure, so ASR server uses correct codec

### [0.1.7](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.7) - 05-06-2020

#### Bug fixes
- Fix `CordovaRecorder` to use event's `data` property instead of `audio`.

### [0.1.6](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.6) - 04-06-2020

#### Bug fixes
- Add Cordova permission request to MediaRecorderWrapper

### [0.1.5](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.5) - 03-06-2020

#### Bug fixes
- Force recognition to run, even when skipRecognition was provided to `stop()`.

### New features
- GrammarInteraction `stop()` now returns a Promise which resolves with the returned `AsrResult`.

### [0.1.4](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.4) - 03-06-2020

#### Bug fixes
- Fix faulty release of source files
- Include check to prevent faulty releases

### [0.1.3](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.3) - 03-06-2020

#### Bug fixes
- Change `@angular/core` peerDependency version to allow `>= 6.0.0 < 10.0.0`

### [0.1.2](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.2) - 03-06-2020

#### Bug fixes
- Release fixed `uuid` version constraint and accept all versions between 1.2.0 and 9.0.0

### [0.1.1](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.1) - 03-06-2020

#### Bug fixes
- Prevent duplicate event listener binding when setting up a new Transport Connection.

### [0.1.0](https://www.npmjs.com/package/@novo-learning/speech/v/0.1.0) - 02-06-2020
- Initial release