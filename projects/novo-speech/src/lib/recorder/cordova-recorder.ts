import { Deferred } from '../models/utilities';
import { CodecType, RecorderInterface } from './util/recorder.interface';

declare global {
    interface Window {
        /**
         * @see {https://github.com/jammind/cordova-audioinput/blob/master/www/audioInputCapture.js}
         */
        audioinput: any;

        cordova?: any;
        device?: { platform: string };
    }
}

// @dynamic
export class CordovaRecorder implements RecorderInterface {

    readonly codec: CodecType = {
        type: 'ogg',
        codec: 'speex'
    };

    private bufferLen = 8192;

    private cordovaAudioInput = window.audioinput;
    private readonly sampleRate: number = window.audioinput.SAMPLERATE.VOIP_16000Hz;

    private activeOnAudioClb?: (data: { data: Blob }) => void;

    private recording = false;

    static ensurePermissions(): Promise<void> {
        return new Promise((res, rej) => {
            if (!window.cordova?.plugins?.diagnostic) {
                rej(`Can't request permissions since Cordova Diagnostic Plugin is not available. Make sure to include the plugin in your project.`);
                return;
            }
            window.cordova.plugins.diagnostic.requestMicrophoneAuthorization((status: any) => {
                (status !== window.cordova.plugins.diagnostic.permissionStatus.GRANTED) ? rej({ name: `CORDOVA.MICROPHONE.PERMISSION.${status?.toUpperCase()}` }) : res();
            }, (error: any) => rej(error));
        });
    }

    /**
     * Creates an event handler which can be attached on the
     * 'audioinput' event produced by Cordova AudioInput
     */
    private cordovaAudioClb(clb: (audio: Blob) => void): ((data: { data: Blob }) => void) {
        return (data) => this.recording ? clb(data.data) : void 0;
    }

    constructor() {
        if (!('cordova' in window)) {
            throw new Error(`CordovaRecorder requires Cordova to be available.`);
        }

        // Make a short test recording
        // This has the side effect of starting an AVAudioSession on iOS
        // which makes subsequent calls to start recording faster
        this.cordovaAudioInput.start({
            bufferSize: this.bufferLen,
            sampleRate: this.sampleRate
        });
        setTimeout(() => {
            this.cordovaAudioInput.stop();
        }, 100);

        window.addEventListener('audioinputerror', (err: any) => console.error(err), false);
    }

    reset() {
        this.cordovaAudioInput.start({
            bufferSize: this.bufferLen,
            sampleRate: this.sampleRate
        });
        this.cordovaAudioInput.stop();
    }

    start(clb: (audio: Blob) => void) {
        this.activeOnAudioClb = this.cordovaAudioClb(clb);
        window.addEventListener<any>('audioinput', this.activeOnAudioClb);
        this.cordovaAudioInput.start({
            bufferSize: this.bufferLen,
            sampleRate: this.sampleRate
        });
        this.recording = true;
    }

    stop() {
        this.cordovaAudioInput.stop();
        this.recording = false;
        if (this.activeOnAudioClb) {
            window.removeEventListener<any>('audioinput', this.activeOnAudioClb);
            this.activeOnAudioClb = undefined;
        }
    }

    getCurrentActivityLevel(deferred: Deferred<any>) {
        // NOT IMPLEMENTED
    }
}
