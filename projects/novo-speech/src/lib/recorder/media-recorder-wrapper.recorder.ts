import { CodecType, RecorderInterface } from './util/recorder.interface';

declare var MediaRecorder: any;

// @dynamic
export class MediaRecorderWrapper implements RecorderInterface {

    static get mediaContainer(): 'webm' | 'ogg' {
        if (!MediaRecorder.isTypeSupported('audio/webm')) {
            return 'ogg';
        }
        if (navigator.userAgent.indexOf('Firefox') >= 0) {
            // Firefox uses a newer webm version than currently supported on our server, so fall back to ogg
            return 'ogg';
        }
        return 'webm';
    }

    readonly codec: CodecType = {
        type: MediaRecorderWrapper.mediaContainer,
        codec: 'opus'
    };

    private mediaRecorder: typeof MediaRecorder;
    private mediaRecorderStream?: MediaStream;

    private activeOnAudioClb?: (blob: Blob) => void;
    private bufferingAudio = false;

    /**
     * Request microphone permissions
     */
    static ensurePermissions(): Promise<void> {
        return navigator.mediaDevices.getUserMedia({ audio: true, video: false }).then(() => void 0);
    }

    async setupMediaStream(): Promise<void> {
        this.mediaRecorderStream = await navigator.mediaDevices.getUserMedia({
            audio: { channelCount: 1 } as MediaTrackConstraints
        });
        const mimeType = 'audio/' + MediaRecorderWrapper.mediaContainer;

        this.mediaRecorder = new MediaRecorder(this.mediaRecorderStream, {mimeType});
        this.mediaRecorder.ondataavailable = (e: { data: Blob }) => {
            const blob = new Blob([e.data]);
            if (this.bufferingAudio && blob.size > 0 && this.activeOnAudioClb) {
                this.activeOnAudioClb(blob);
            }
        };
    }

    async start(clb: (audio: Blob) => void) {
        await this.setupMediaStream();
        this.bufferingAudio = true;
        this.activeOnAudioClb = clb;
        this.mediaRecorder.start(250);
    }

    stop() {
        this.bufferingAudio = false;
        this.activeOnAudioClb = undefined;
        try {
            this.mediaRecorder.stop();
        } catch (err) {
            console.error(err);
        }

        this.mediaRecorderStream?.getAudioTracks().forEach(track => track.stop());
        this.mediaRecorderStream = undefined;
    }

    reset() {
        // NOT IMPLEMENTED
    }

    getCurrentActivityLevel() {
        // NOT IMPLEMENTED
    }
}
