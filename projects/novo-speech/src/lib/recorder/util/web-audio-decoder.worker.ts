/**
 * Create a new instance of the audio decoder worker
 */
export const newAudioDecoderWorker = (): Worker => {
    const blobURL = URL.createObjectURL(
        new Blob(
            [
                '(',
                function (this: any) {
                    const that = this;
                    let recLength = 0;
                    let recBuffers: any[] = [];
                    let sampleRate: number;
                    let maxSample = 0;
                    const clippingThreshold = 2;
                    const floatTolerance = 0.0001;
                    let nrMicSamples = 0;
                    let currentActivityLevel = 0;
                    const currentActivityLevels: number[] = [];

                    that.onmessage = (e: MessageEvent) => {
                        switch (e.data.command) {
                            case 'init':
                                sampleRate = e.data.params.sampleRate;
                                break;
                            case 'record':
                                record(e.data.buffer);
                                break;
                            case 'clear':
                                clear();
                                break;
                            case 'getActivityLevel':
                                currentActivityLevels.push(currentActivityLevel);
                                if (currentActivityLevels.length > 4) {
                                    currentActivityLevels.shift();
                                }
                                that.postMessage({
                                    type: 'micVolume',
                                    data: 100 * mean(currentActivityLevels)
                                });
                                break;
                        }
                    };

                    function mean(a: number[]) {
                        const sumLevel = a.reduce((s, c) => s += c, 0);
                        return sumLevel / a.length;
                    }

                    function record(inputBuffer: Float32Array) {
                        clear();
                        recBuffers.push(inputBuffer);
                        recLength += inputBuffer.length;
                        process();
                    }

                    function process() {
                        const buffer = mergeBuffers(recBuffers, recLength);
                        const factor = 16000 / sampleRate;
                        const bufLen = Math.ceil(factor * buffer.length);
                        const arraybuffer = new ArrayBuffer(bufLen * 2);
                        const view = new DataView(arraybuffer);
                        format(view, 0, buffer, factor);

                        that.postMessage({
                            type: 'data',
                            data: view.buffer
                        });
                    }

                    function clear() {
                        recLength = 0;
                        recBuffers = [];
                    }

                    function mergeBuffers(rbs: Float32Array[], rl: number): Float32Array {
                        const result = new Float32Array(rl);
                        let offset = 0;
                        for (const rb of rbs) {
                            result.set(rb, offset);
                            offset += rb.length;
                        }
                        return result;
                    }

                    function format(output: DataView, offset: number, input: Float32Array, factor: number) {
                        let i = 0;
                        const skipSamples = 1.0 / factor;
                        let i2 = skipSamples;
                        let sumSquares = 0;
                        let nrSamples = 0;
                        let nrMaxSamples = 0;
                        let clipping = false;

                        while (i < input.length) {
                            nrSamples += 1;
                            nrMicSamples += 1;
                            let sample = 0;
                            while (i < i2) {
                                i++;
                            }
                            sample = Math.max(-1, Math.min(1, input[i - 1]));
                            if (!isNaN(sample)) {
                                sumSquares += Math.pow(sample, 2);
                                if (nrMicSamples > 500) {
                                    currentActivityLevel = Math.sqrt(sumSquares / nrMicSamples);
                                    sumSquares = 0;
                                    nrMicSamples = 0;
                                }

                                if (Math.abs(sample - maxSample) < floatTolerance && maxSample > 0) {
                                    nrMaxSamples += 1;
                                } else {
                                    nrMaxSamples = 0;
                                }

                                if (sample > maxSample) {
                                    maxSample = sample;
                                }

                                if (nrMaxSamples >= clippingThreshold) {
                                    // clipping
                                    clipping = true;
                                    nrMaxSamples = 0;
                                }

                                output.setInt16(offset, sample < 0 ? sample * 0x8000 : sample * 0x7fff, true);
                                offset += 2;
                                i2 += skipSamples;
                            }
                        }

                        if (clipping) {
                            that.postMessage({ type: 'micClipping' });
                        }
                    }
                }.toString(),
                ')()'
            ],
            { type: 'application/javascript' }
        )
    );
    const worker = new Worker(blobURL);
    URL.revokeObjectURL(blobURL);

    return worker;
};
