export interface RecorderInterface {
    readonly codec: CodecType | null;

    start(clb: (audio: Blob) => void): void;
    stop(): void;
    getCurrentActivityLevel(d: any): void;
    reset(): void;
}

export interface CodecType {
    type: 'webm' | 'ogg';
    codec: 'opus' | 'speex' | null;
}
