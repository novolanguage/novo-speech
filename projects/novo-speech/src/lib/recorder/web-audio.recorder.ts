import { Deferred } from '../models/utilities';
import { CodecType, RecorderInterface } from './util/recorder.interface';
import { newAudioDecoderWorker } from './util/web-audio-decoder.worker';

// @dynamic
export class WebAudioRecorder implements RecorderInterface {
    readonly codec: CodecType | null = null;

    private bufferLen = 8192;
    private sampleRate: number;
    private recorderWorker: Worker = newAudioDecoderWorker();
    private audioContext: AudioContext = this.getAudioContext();

    private recording = false;
    private onMicClipping: () => void;
    private gettingMicInputLevel?: Deferred<void>;
    private node: ScriptProcessorNode;
    private webaudioStream?: MediaStream;
    private source?: MediaStreamAudioSourceNode;

    private activeOnAudioClb?: (audio: Blob) => void;

    /**
     * Request microphone permissions
     */
    static ensurePermissions(): Promise<void> {
        return navigator.mediaDevices.getUserMedia({ audio: true, video: false }).then(() => void 0);
    }

    async setupMediaStream() {
        this.webaudioStream  = await navigator.mediaDevices.getUserMedia({
            audio: { channelCount: 1 } as MediaTrackConstraints
        });
        this.source = this.audioContext.createMediaStreamSource(this.webaudioStream);
        this.source.connect(this.node);
    }

    constructor(onMicClipping: () => void) {
        this.onMicClipping = onMicClipping;

        this.node = this.audioContext.createScriptProcessor(this.bufferLen, 1, 1);
        this.node.connect(this.audioContext.destination);
        this.node.addEventListener('audioprocess', e => {
            if (this.recording) {
                this.recorderWorker.postMessage({
                    command: 'record',
                    buffer: e.inputBuffer.getChannelData(0)
                });
            }
        });
        this.sampleRate = this.audioContext.sampleRate;


        this.recorderWorker.onmessage = e => {
            switch (e.data.type) {
                case 'data':
                    if (this.recording && this.activeOnAudioClb) {
                        this.activeOnAudioClb(e.data.data);
                    }
                    break;
                case 'micVolume':
                    if (this.gettingMicInputLevel) {
                        this.gettingMicInputLevel.resolve(e.data.data);
                    }
                    break;
                case 'micClipping':
                    this.onMicClipping();
                    break;
                case 'debug':
                    break;
            }
        };

        this.recorderWorker.onerror = e => { };

        this.recorderWorker.postMessage({
            command: 'init',
            params: {
                sampleRate: this.sampleRate
            }
        });
    }

    reset() {
        // NOT IMPLEMENTED
    }

    async start(clb: (audio: Blob) => void) {
        await this.setupMediaStream();
        this.clear();
        this.activeOnAudioClb = clb;
        this.recording = true;
    }

    stop() {
        this.source?.disconnect();
        this.webaudioStream?.getAudioTracks().forEach(track => track.stop());
        
        this.activeOnAudioClb = undefined;
        this.recording = false;
    }

    clear() {
        this.recorderWorker.postMessage({ command: 'clear' });
    }

    getCurrentActivityLevel(deferred: Deferred<void>) {
        this.gettingMicInputLevel = deferred;
        this.recorderWorker.postMessage({ command: 'getActivityLevel' });
    }

    private getAudioContext(): AudioContext {
        // tslint:disable-next-line:no-string-literal
        window['AudioContext'] = window['AudioContext'] || (window as any)['webkitAudioContext'];
        return new AudioContext();
    }
}
