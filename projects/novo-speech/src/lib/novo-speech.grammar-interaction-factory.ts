import { EventBus } from './events/novo-speech.event-bus';
import { AsrGrammarWord, ConfusionGrammarData, ConfusionGrammarGroup, ConfusionNetworkGrammar, OpenRecordingGrammar } from './models/grammar';
import { LanguageCode, phonesetMap } from './models/phone-set';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import { SessionManager } from './novo-speech.session-manager';
import { RecorderInterface } from './recorder/util/recorder.interface';

export const multipleChoiceGrammarFactory = (
    options: string[],
    returnIntermediateResults = false,
    language: LanguageCode,
    session: SessionManager,
    recorder: RecorderInterface | undefined,
    eventBus: EventBus,
): NovoSpeechGrammarInteraction => {
    const choiceGrammar = new ConfusionNetworkGrammar();
    choiceGrammar.data = new ConfusionGrammarData();
    choiceGrammar.data.kind = 'alternatives';
    choiceGrammar.data.elements = options.map(opt => {
        const group = new ConfusionGrammarGroup();
        group.kind = 'sequence';
        const words = opt.split(/\s+/);
        group.elements = words.map(w => new AsrGrammarWord(w));
        return group;
    });
    choiceGrammar.return_objects = ['dict'];
    choiceGrammar.phoneset = phonesetMap[language];

    return new NovoSpeechGrammarInteraction(choiceGrammar, returnIntermediateResults, session, recorder, eventBus);
};

export const forcedAlignmentGrammarFactory = (
    text: string | string[],
    returnIntermediateResults = false,
    language: LanguageCode,
    session: SessionManager,
    recorder: RecorderInterface | undefined,
    eventBus: EventBus,
): NovoSpeechGrammarInteraction => {
    const grammar = new ConfusionNetworkGrammar();
    const chunks: string[] = typeof text === 'string' ? text.split(/\s+/) : text;
    grammar.data = new ConfusionGrammarData();
    grammar.data.kind = 'sequence';
    grammar.data.elements = chunks.map(chunk => {
        return new AsrGrammarWord(chunk);
    });
    grammar.return_objects = ['dict'];
    grammar.phoneset = phonesetMap[language];

    return new NovoSpeechGrammarInteraction(grammar, returnIntermediateResults, session, recorder, eventBus);
};

export const openRecordingGrammarFactory = (
    session: SessionManager,
    recorder: RecorderInterface | undefined,
    eventBus: EventBus,
): NovoSpeechGrammarInteraction => {
    const grammar = new OpenRecordingGrammar();
    return new NovoSpeechGrammarInteraction(grammar, false, session, recorder, eventBus);
};