import { EventBus } from './events/novo-speech.event-bus';
import { AsrGrammarWord, ConfusionGrammarGroup, ConfusionNetworkGrammar, OpenRecordingGrammar, phonesetMap } from './models';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import { forcedAlignmentGrammarFactory, multipleChoiceGrammarFactory, openRecordingGrammarFactory } from './novo-speech.grammar-interaction-factory';
import { SessionManager } from './novo-speech.session-manager';
import { MediaRecorderWrapper } from './recorder/media-recorder-wrapper.recorder';
import { RecorderInterface } from './recorder/util/recorder.interface';

describe('GrammarInteractionFactory', () => {
    let sm: SessionManager;
    let ri: RecorderInterface;
    let eb: EventBus;

    beforeEach(() => {
        eb = new EventBus();
        ri = new MediaRecorderWrapper();
        sm = new SessionManager('abc', 'http://test.com', 'nl', ri.codec || undefined, undefined, eb);
    });

    describe('multipleChoiceGrammarFactory', () => {
        let gi: NovoSpeechGrammarInteraction;

        beforeEach(() => {
            gi = multipleChoiceGrammarFactory(['this', 'that'], true, 'nl', sm, ri, eb);
        });

        it('creates a new NovoSpeechGrammarInteraction instance', () => {
            expect(gi).toBeInstanceOf(NovoSpeechGrammarInteraction);
        });

        it('should have created a ConfusionNetworkGrammar with each option defined as ConfusionGrammarGroup', () => {
            expect(gi.grammar).toBeInstanceOf(ConfusionNetworkGrammar);

            const elements = (gi.grammar as ConfusionNetworkGrammar).data.elements;

            expect(elements[0]).toBeInstanceOf(ConfusionGrammarGroup);
            expect(elements[1]).toBeInstanceOf(ConfusionGrammarGroup);

            const groupOne = (elements[0] as ConfusionGrammarGroup);
            expect(groupOne.kind).toEqual('sequence');
            expect(groupOne.elements).toEqual([new AsrGrammarWord('this')]);

            const groupTwo = (elements[1] as ConfusionGrammarGroup);
            expect(groupTwo.kind).toEqual('sequence');
            expect(groupTwo.elements).toEqual([new AsrGrammarWord('that')]);
        });

        it('should have set return_objects to "dict"', () => {
            expect((gi.grammar as ConfusionNetworkGrammar).return_objects).toEqual(['dict']);
        });

        it('should have set the correct phoneset based on given target language', () => {
            // tslint:disable-next-line: no-non-null-assertion
            expect((gi.grammar as ConfusionNetworkGrammar).phoneset).toEqual(phonesetMap.nl!);
        });
    });

    describe('forcedAlignmentGrammarFactory', () => {
        let gi: NovoSpeechGrammarInteraction;

        beforeEach(() => {
            gi = forcedAlignmentGrammarFactory('on a boat', true, 'nl', sm, ri, eb);
        });

        it('creates a new NovoSpeechGrammarInteraction instance', () => {
            expect(gi).toBeInstanceOf(NovoSpeechGrammarInteraction);
        });

        it('should have created a ConfusionNetworkGrammar with each word defined as element', () => {
            expect(gi.grammar).toBeInstanceOf(ConfusionNetworkGrammar);

            const cng = (gi.grammar as ConfusionNetworkGrammar).data;

            expect(cng.kind).toEqual('sequence');
            expect(cng.elements).toEqual([
                new AsrGrammarWord('on'),
                new AsrGrammarWord('a'),
                new AsrGrammarWord('boat'),
            ]);
        });

        it('should have set return_objects to "dict"', () => {
            expect((gi.grammar as ConfusionNetworkGrammar).return_objects).toEqual(['dict']);
        });

        it('should have set the correct phoneset based on given target language', () => {
            // tslint:disable-next-line: no-non-null-assertion
            expect((gi.grammar as ConfusionNetworkGrammar).phoneset).toEqual(phonesetMap.nl!);
        });
    });

    describe('openRecordingGrammarFactory', () => {
        let gi: NovoSpeechGrammarInteraction;

        beforeEach(() => {
            gi = openRecordingGrammarFactory(sm, ri, eb);
        });

        it('creates a new NovoSpeechGrammarInteraction instance', () => {
            expect(gi).toBeInstanceOf(NovoSpeechGrammarInteraction);
        });

        it('should have created an OpenRecordingGrammar grammar', () => {
            expect(gi.grammar).toBeInstanceOf(OpenRecordingGrammar);
        });
    });

});
