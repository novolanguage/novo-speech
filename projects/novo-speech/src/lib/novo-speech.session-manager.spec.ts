import { events, TransportLostConnectionEvent } from './events/events.constants';
import { ChanneledEventBus, EventBus } from './events/novo-speech.event-bus';
import { AsrSession, NovoSpraakError, NovoSpraakErrorCode } from './models';
import { AsrSessionState } from './novo-speech.interfaces';
import { CTX_ID_STORAGE_KEY, NovoSpraakRequestResult, SessionManager } from './novo-speech.session-manager';
import { NovoSpeechTransport } from './novo-speech.transport';
import * as sessionData from './__mock__/create-session.json';

describe('SessionManager', () => {
    let sm: SessionManager;
    let eb: EventBus;

    beforeEach(() => {
        eb = new EventBus();
        sm = new SessionManager('abc-123', 'https://mock-api.novo/api', 'nl', undefined, undefined, eb);
    });

    it('sets the provided token as Authentication-Token in the headers object', () => {
        const setSpy = spyOn(SessionManager.prototype, 'setAuthenticationToken');
        // tslint:disable-next-line: no-unused-expression
        new SessionManager('abc-123', 'https://mock-api.novo/api', 'nl', undefined);

        expect(setSpy).toHaveBeenCalledWith('abc-123');
    });

    describe('state', () => {

        it('returns _internalState', () => {
            (sm as any)._internalState = AsrSessionState.StartingSession;

            expect(sm.state).toEqual(AsrSessionState.StartingSession);
        });

    });

    describe('#hasValidSession', () => {

        it('returns false when activeSession is not defined', () => {
            expect(sm.hasValidSession()).toBeFalse();
        });

        it('returns true when activeSession is defined', () => {
            sm.activeSession = {} as AsrSession;

            expect(sm.hasValidSession()).toBeTrue();
        });

    });

    describe('#establish', () => {
        let callSpy: jasmine.Spy;
        let handleSpy: jasmine.Spy;

        beforeEach(() => {
            // Make setTimeout calls synchronous
            spyOn<any>(window, 'setTimeout').and.callFake((fn: (...args: any[]) => void, ...args: any[]) => {
                fn.apply(null, [].slice.call(args, 2));
                return +new Date();
            });

            callSpy = spyOn<any>(sm, 'call').and.returnValues(
                Promise.resolve<NovoSpraakRequestResult>({
                    data: {},
                    headers: {
                        location: 'https://mock-gm.novo/v1/abcdef-1234567'
                    },
                    status: 'success'
                }),
                Promise.resolve<NovoSpraakRequestResult>({
                    data: sessionData,
                    headers: {},
                    status: 'success'
                })
            );

            handleSpy = spyOn<any>(sm, 'handleNewSession').and.resolveTo();
        });

        it('returns pendingSessionPromise when already set', async () => {
            const prom = Promise.resolve('foobar');
            (sm as any).pendingSessionPromise = prom;

            await expectAsync<any, any>(sm.establish()).toBeResolvedTo('foobar');
        });

        it('returns transport when isValid() returns true', async () => {
            const tl = new NovoSpeechTransport('test');
            sm.transport = tl;
            spyOn(sm, 'isValid').and.returnValue(true);

            await expectAsync(sm.establish()).toBeResolvedTo(tl);
        });

        it('invalidates the existing session', async () => {
            const invalidateSpy = spyOn<any>(sm, 'invalidateSession').and.resolveTo(true);

            await sm.establish();

            expect(invalidateSpy).toHaveBeenCalled();
        });

        it('calls abortAllRequests()', async () => {
            const abortSpy = spyOn<any>(sm, 'abortAllRequests');

            await sm.establish();

            expect(abortSpy).toHaveBeenCalled();
        });

        it('should create a new session on the remote API', async () => {
            await sm.establish();

            expect(callSpy.calls.allArgs()).toEqual([
                ['/sessions', jasmine.any(Object)],
                ['https://mock-gm.novo/v1/abcdef-1234567', null, 'GET', false],
            ]);
        });

        it('should pass the returned session data to handleNewSession', async () => {
            await sm.establish();

            expect(handleSpy).toHaveBeenCalledWith(sessionData.result[0], undefined);
        });

        it('should fail when no location was returned', async () => {
            callSpy.and.returnValue(
                Promise.resolve<NovoSpraakRequestResult>({
                    data: {},
                    headers: {
                        location: undefined
                    },
                    status: 'success'
                }),
            );

            await expectAsync(sm.establish()).toBeRejectedWithError('Expected to receive a Location to poll, but none was given');
        });

        it('should fail when POST /session request fails', async () => {
            callSpy.and.returnValue(
                Promise.reject<NovoSpraakError>(new NovoSpraakError('Failure', NovoSpraakErrorCode.UnknownError)),
            );

            await expectAsync(sm.establish()).toBeRejectedWithError('Failure');
        });

    });

    describe('#handleNewSession', () => {
        let transportConnectSpy: jasmine.Spy;
        let stateSpy: jasmine.Spy;

        beforeEach(() => {
            transportConnectSpy = spyOn(NovoSpeechTransport.prototype, 'connect').and.resolveTo();
            stateSpy = spyOnProperty(sm, 'state', 'set');
        });

        it('should throw an error when no sessiondata is provided', async () => {
            await expectAsync(sm.handleNewSession(null)).toBeRejectedWithError(`No session. Error: `);
        });

        it('sets the state to AsrSessionState.StartingSessionFailed when an error is thrown ', async () => {
            await sm.handleNewSession(null).catch(() => void 0);

            expect(stateSpy).toHaveBeenCalledWith(AsrSessionState.StartingSessionFailed);
        });

        it('should throw an error when sessiondata contains the error property', async () => {
            await expectAsync(sm.handleNewSession({ error: 'Oh no!' })).toBeRejectedWithError(`No session. Error: Oh no!`);
        });

        it('should initialize a new AsrSession', async () => {
            const initSpy = spyOn(AsrSession, 'initialize').and.callThrough();

            await sm.handleNewSession(sessionData.result[0]);

            expect(initSpy).toHaveBeenCalled();
        });

        it('should setup a new Transport instance with the sessions proxy URL', async () => {
            const transportSpy = spyOn<any>(sm, 'setupTransportConnection').and.callThrough();

            await sm.handleNewSession(sessionData.result[0]);

            expect(transportSpy).toHaveBeenCalledWith('wss://proxy-do-ams3.novo-learning.com/ca0892bb-9d46-4be0-b0fb-658a3bb46cf3');
        });

        it('calls initCodec() on the Transport instance when a codec was provided', async () => {
            const codecSpy = spyOn(NovoSpeechTransport.prototype, 'initCodec');

            await sm.handleNewSession(sessionData.result[0], {
                codec: 'opus',
                type: 'ogg'
            });

            expect(codecSpy).toHaveBeenCalledWith({
                codec: 'opus',
                type: 'ogg'
            });
        });

        it('sets the state to AsrSessionState.StartedSession ', async () => {
            await sm.handleNewSession(sessionData.result[0]);

            expect(stateSpy).toHaveBeenCalledWith(AsrSessionState.StartedSession);
        });

        it('returns an instance of NovoSpeechTransport', async () => {
            const transport = await sm.handleNewSession(sessionData.result[0]);

            expect(transport).toBeInstanceOf(NovoSpeechTransport);
        });
    });

    describe('#isValid', () => {

        beforeEach(() => {
            sm.activeSession = AsrSession.initialize({}, 'nl');
            sm.transport = new NovoSpeechTransport('ws://test.com', eb);
            sm.lastMessageDate = new Date();
        });

        it('should be marked as valid when activeSession matches the sNodeID and Language and transport and lastMessageDate are set', () => {
            expect(sm.isValid()).toBeTrue();
        });

        it('should be invalid when activeSession is undefined', () => {
            sm.activeSession = undefined;

            expect(sm.isValid()).toBeFalse();
        });

        it('should be invalid when activeSession sNodeId does not match SessionManager sNodeId', () => {
            sm.activeSession = AsrSession.initialize({ snode: { id: 100, datacentre: { proxy: { hostname: 'bla', ip: '8.8.4.4' } } } }, 'nl');

            expect(sm.isValid()).toBeFalse();
        });

        it('should be invalid when activeSession targetLanguage does not match SessionManager language', () => {
            sm.activeSession = AsrSession.initialize({}, 'en');

            expect(sm.isValid()).toBeFalse();
        });

        it('should be invalid when transport is undefined', () => {
            sm.transport = undefined;

            expect(sm.isValid()).toBeFalse();
        });

        it('should be valid when lastMessageDate is undefined', () => {
            sm.lastMessageDate = undefined;

            expect(sm.isValid()).toBeTrue();
        });

        it('should be valid when lastMessageDate was set in less than 60 minutes', () => {
            sm.lastMessageDate = new Date(new Date().getTime() - (1000 * 60 * 30));

            expect(sm.isValid()).toBeTrue();
        });

        it('should be invalid when lastMessageDate was set more than 60 minutes ago', () => {
            sm.lastMessageDate = new Date(new Date().getTime() - (1000 * 60 * 65));

            expect(sm.isValid()).toBeFalse();
        });

    });

    describe('#checkConnection', () => {

        it('establishes connection when no transport is available', async () => {
            const transport = new NovoSpeechTransport('ws://test.com');
            const checkConnSpy = spyOn(transport, 'checkConnection').and.resolveTo();

            sm.transport = undefined;

            const establishSpy = spyOn(sm, 'establish').and.resolveTo(transport);
            await expectAsync(sm.checkConnection()).toBeResolved();

            expect(checkConnSpy).toHaveBeenCalledTimes(1);
            expect(establishSpy).toHaveBeenCalledTimes(1);
        });

        it('calls checkConnection on set transport instance', async () => {
            sm.transport = new NovoSpeechTransport('ws://test.com');
            const checkConnSpy = spyOn(sm.transport, 'checkConnection').and.resolveTo();

            await expectAsync(sm.checkConnection()).toBeResolved();
            expect(checkConnSpy).toHaveBeenCalledTimes(1);
        });
    });

    describe('#end', () => {
        let callSpy: jasmine.Spy;

        beforeEach(() => {
            sm.activeSession = AsrSession.initialize({ id: 1001 }, 'nl');
            callSpy = spyOn<any>(sm, 'call').and.returnValue(
                Promise.resolve<NovoSpraakRequestResult>({
                    data: {},
                    headers: {},
                    status: 'success'
                })
            );
        });

        it('resolves when activeSession is undefined', async () => {
            sm.activeSession = undefined;

            await expectAsync(sm.end()).toBeResolved();
            expect(callSpy).not.toHaveBeenCalled();
        });

        it('calls abortAllRequests', async () => {
            const abortSpy = spyOn<any>(sm, 'abortAllRequests');

            await sm.end();

            expect(abortSpy).toHaveBeenCalled();
        });

        it('deletes the session on the remote server', async () => {
            await sm.end();

            expect(callSpy).toHaveBeenCalledWith(`/sessions/1001`, {}, 'DELETE');
        });

        it('calls invalidateSession()', async () => {
            const invalidateSpy = spyOn<any>(sm, 'invalidateSession');

            await sm.end();

            expect(invalidateSpy).toHaveBeenCalled();
        });
    });

    describe('constructor', () => {
        let eventSpy: jasmine.Spy;
        let msgSpy: jasmine.Spy;
        let channeledEb: ChanneledEventBus;

        beforeEach(() => {
            // setup mock and create new SM instance to test constructor
            channeledEb = new EventBus(eb) as ChanneledEventBus;
            spyOn(eb, 'channel').and.returnValue(channeledEb);

            eventSpy = spyOn(channeledEb, 'addEventListener').and.callThrough();
            msgSpy = spyOn<any>(SessionManager.prototype, 'messageReceived');

            sm = new SessionManager('abc-123', 'https://mock-api.novo/api', 'nl', undefined, undefined, eb);
        });

        it('sets up an eventListener listening for lost connection events', () => {
            expect(eventSpy).toHaveBeenCalledWith(events.TRANSPORT_LOST_CONNECTION, jasmine.any(Function));
        });

        it('sets up an eventListener listening for received messages events', () => {
            expect(eventSpy).toHaveBeenCalledWith(events.TRANSPORT_MESSAGE_RECEIVED, jasmine.any(Function));
        });

        it('calls messageReceived() when a TRANSPORT_MESSAGE_RECEIVED is received', () => {
            sm.eventBus.dispatch(events.TRANSPORT_MESSAGE_RECEIVED);

            expect(msgSpy).toHaveBeenCalled();
        });

    });

    describe('#setupTransportConnection', () => {

        beforeEach(() => {
            // setup transport mock
            spyOn(NovoSpeechTransport.prototype, 'connect').and.resolveTo();
        });

        it('calls invalidateSession() when TRANSPORT_LOST_CONNECTION is fired', () => {
            const invalidateSpy = spyOn<any>(sm, 'invalidateSession');
            const establishSpy = spyOn(sm, 'establish').and.returnValue(Promise.resolve(sm.transport!));

            (sm as any).setupTransportConnection('ws://test.com');
            sm.eventBus.dispatch<TransportLostConnectionEvent>(events.TRANSPORT_LOST_CONNECTION, { code: 20, reason: 'nope' });

            expect(invalidateSpy).toHaveBeenCalled();
            expect(establishSpy).toHaveBeenCalled();
        });

        it('does not call establish() when TRANSPORT_LOST_CONNECTION is fired with code 1001', () => {
            spyOn<any>(sm, 'invalidateSession');
            const establishSpy = spyOn(sm, 'establish');

            (sm as any).setupTransportConnection('ws://test.com');
            eb.dispatch<TransportLostConnectionEvent>(events.TRANSPORT_LOST_CONNECTION, { code: 1001, reason: 'nope' });

            expect(establishSpy).not.toHaveBeenCalled();
        });

    });


    describe('#messageReceived', () => {

        it('sets the current date', () => {
            const date = new Date();
            jasmine.clock().mockDate(date);

            (sm as any).messageReceived();

            expect(sm.lastMessageDate).toEqual(date);
        });

    });

    describe('#getContextId', () => {
        let sessionSpy: jasmine.Spy;
        let localSpy: jasmine.Spy;

        beforeEach(() => {
            delete window.cordova;

            sessionSpy = spyOn(window.sessionStorage, 'getItem').and.returnValue(null);
            localSpy = spyOn(window.localStorage, 'getItem').and.returnValue(null);
        });

        afterEach(() => {
            delete window.cordova;
        });

        it('uses sessionStorage when cordova is not registered', () => {
            (sm as any).getContextId();

            expect(sessionSpy).toHaveBeenCalled();
        });

        it('uses localStorage when cordova is registered', () => {
            window.cordova = {};

            (sm as any).getContextId();

            expect(localSpy).toHaveBeenCalled();
        });

        it('returns the contextId from storage', () => {
            sessionSpy.and.returnValue('123');

            const id = (sm as any).getContextId();

            expect(id).toEqual('123');
        });

        it('creates a new contextId when none is available', () => {
            sessionSpy.and.returnValue(null);

            const id = (sm as any).getContextId();

            expect(id.length).toEqual(36);
        });

        it('stores the newly generated contextID', () => {
            sessionSpy.and.returnValue(null);
            const storeSpy = spyOn(window.sessionStorage, 'setItem');

            const id = (sm as any).getContextId();

            expect(storeSpy).toHaveBeenCalledWith(CTX_ID_STORAGE_KEY, id);
        });
    });

    describe('#call', () => {
        let fetchSpy: jasmine.Spy;

        beforeEach(() => {
            fetchSpy = spyOn(window, 'fetch').and.resolveTo(new Response());
        });

        it('should throw an Error when no authentication token is set', async () => {
            sm.setAuthenticationToken(null as any); // force token to be null

            await expectAsync((sm as any).call('/resource', {}, 'GET')).toBeRejectedWithError('No authentication token set');
        });

        it('includes the base by default', () => {
            (sm as any).call('/resource', null, 'GET');

            expect(fetchSpy).toHaveBeenCalledWith('https://mock-api.novo/api/resource', jasmine.any(Object));
        });

        it('does not include the base when the last param is set to false', () => {
            (sm as any).call('/resource', null, 'GET', false);

            expect(fetchSpy).toHaveBeenCalledWith('/resource', jasmine.any(Object));
        });

        it('adds the body as query string when method equals GET', () => {
            (sm as any).call('/resource', { a: true }, 'GET');

            expect(fetchSpy).toHaveBeenCalledWith('https://mock-api.novo/api/resource?a=true', jasmine.any(Object));
        });

        it('adds the parameter as stringified json when method does not equal GET', () => {
            (sm as any).call('/resource', { a: true }, 'POST');

            expect(fetchSpy).toHaveBeenCalledWith('https://mock-api.novo/api/resource', jasmine.objectContaining({
                body: JSON.stringify({ a: true })
            }));
        });

        it('adds the AbortController signal', () => {
            (sm as any).call('/resource', null, 'GET');

            expect(fetchSpy).toHaveBeenCalledWith(jasmine.any(String), jasmine.objectContaining({
                signal: (sm as any).abortCtrl.signal
            }));
        });

        it('includes the method', () => {
            (sm as any).call('/resource', null, 'GET');

            expect(fetchSpy).toHaveBeenCalledWith(jasmine.any(String), jasmine.objectContaining({
                method: 'GET'
            }));
        });

        it('includes the headers', () => {
            (sm as any).call('/resource', null, 'GET');

            expect(fetchSpy).toHaveBeenCalledWith(jasmine.any(String), jasmine.objectContaining({
                headers: (sm as any).headers
            }));
        });

        it('throws an error when response was not ok', async () => {
            fetchSpy.and.resolveTo(new Response(null, {
                status: 400,
                statusText: 'failure'
            }));

            await expectAsync((sm as any).call('/resource', null, 'GET')).toBeRejectedWithError('failure');
        });

        it('returns an object containing the response data, status and location header', async () => {
            fetchSpy.and.resolveTo(new Response(JSON.stringify({ b: true }), {
                status: 200,
                statusText: 'OK',
                headers: {
                    Location: 'Somewhere in the ocean'
                }
            }));

            await expectAsync((sm as any).call('/resource', null, 'GET')).toBeResolvedTo({
                status: 'OK',
                data: { b: true },
                headers: {
                    location: 'Somewhere in the ocean'
                }
            });
        });

        it('sets Location to "Unknown" when not available in headers', async () => {
            fetchSpy.and.resolveTo(new Response(JSON.stringify({ b: true }), {
                status: 200,
                statusText: 'OK'
            }));

            await expectAsync((sm as any).call('/resource', null, 'GET')).toBeResolvedTo(jasmine.objectContaining({
                headers: {
                    location: 'Unknown'
                }
            }));
        });
    });
});
