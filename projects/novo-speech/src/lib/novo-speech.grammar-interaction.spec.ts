import { classToPlain } from 'class-transformer';
import { first, skip, take } from 'rxjs/operators';
import { events, GrammarInteractionStateChangedEvent, TransportLostConnectionEvent } from './events/events.constants';
import { AsrResult } from './models';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import { openRecordingGrammarFactory } from './novo-speech.grammar-interaction-factory';
import { AsrGrammarInteractionState } from './novo-speech.interfaces';
import { SessionManager } from './novo-speech.session-manager';
import { NovoSpeechTransport } from './novo-speech.transport';
import { MediaRecorderWrapper } from './recorder/media-recorder-wrapper.recorder';
import { RecorderInterface } from './recorder/util/recorder.interface';

describe('NovoSpeechGrammarInteraction', () => {
    let gi: NovoSpeechGrammarInteraction;
    let ri: RecorderInterface;
    let sm: SessionManager;
    let tm: NovoSpeechTransport;
    let transportRequestSpy: jasmine.Spy;

    beforeEach(() => {
        ri = new MediaRecorderWrapper();
        sm = new SessionManager('abc', 'test.com', 'nl', ri.codec || undefined);
        sm.transport = tm = new NovoSpeechTransport('ws://test.com', sm.eventBus);
        transportRequestSpy = spyOn(tm, 'request').and.resolveTo();

        gi = openRecordingGrammarFactory(sm, ri, sm.eventBus);
    });

    it('should setup an event listener which stops recording when the connection is lost', () => {
        const recordingSpy = spyOn(gi, 'isRecording').and.returnValue(true);
        const stopSpy = spyOn(gi, 'stop');

        gi.eventBus.dispatch<TransportLostConnectionEvent>(events.TRANSPORT_LOST_CONNECTION, { code: 20, reason: 'nope' });

        expect(recordingSpy).toHaveBeenCalled();
        expect(stopSpy).toHaveBeenCalledWith(true);
    });

    it('should have set a grammarCtxId during construct', () => {
        expect(gi.grammarCtxId).not.toBeNull();
    });

    describe('state', () => {

        it('should return a default state of AsrGRammerInteractionState.READY', () => {
            expect(gi.state).toEqual(AsrGrammarInteractionState.READY);
        });

        it('should return _internalState', () => {
            (gi as any)._internalState = 'bla';

            // tslint:disable-next-line: no-unused-expression
            const val = gi.state;

            expect(val).toEqual('bla');
        });

        it('dispatches a GRAMMAR_INTERACTION_STATE_CHANGED event when a new value is set', () => {
            const dispatchSpy = spyOn(gi.eventBus, 'dispatch');

            gi.state = AsrGrammarInteractionState.RECOGNIZING;

            expect(dispatchSpy).toHaveBeenCalledWith(events.GRAMMAR_INTERACTION_STATE_CHANGED, {
                ctx: gi.grammarCtxId,
                state: AsrGrammarInteractionState.RECOGNIZING,
                previous: AsrGrammarInteractionState.READY
            } as GrammarInteractionStateChangedEvent);
        });

        it('does not disptach a GRAMMAR_INTERACTION_STATE_CHANGED event when the new state is the same', () => {
            const dispatchSpy = spyOn(gi.eventBus, 'dispatch');

            gi.state = gi.state;

            expect(dispatchSpy).not.toHaveBeenCalled();
        });

    });

    describe('state$', () => {

        it('adds an eventListener for GRAMMAR_INTERACTION_STATE_CHANGED events on subscription', () => {
            const eventSpy = spyOn(gi.eventBus, 'addEventListener');

            gi.state$.subscribe();

            expect(eventSpy).toHaveBeenCalledWith(events.GRAMMAR_INTERACTION_STATE_CHANGED, jasmine.any(Function), undefined);
        });

        it('filters events with different ctx ID', async () => {
            const resultPromise = gi.state$.pipe(
                skip(1), // ignore the first value
                take(1)  // take the first emission
            ).toPromise();

            gi.eventBus.dispatch<GrammarInteractionStateChangedEvent>(events.GRAMMAR_INTERACTION_STATE_CHANGED, {
                ctx: 'something else',
                state: AsrGrammarInteractionState.RECOGNIZING
            });
            gi.eventBus.dispatch<GrammarInteractionStateChangedEvent>(events.GRAMMAR_INTERACTION_STATE_CHANGED, {
                ctx: gi.grammarCtxId,
                state: AsrGrammarInteractionState.RECORDING
            });

            await expectAsync(resultPromise).toBeResolvedTo(AsrGrammarInteractionState.RECORDING);
        });

    });

    describe('#active', () => {
        let setGrammarSpy: jasmine.Spy;

        beforeEach(() => {
            setGrammarSpy = spyOn(tm, 'setGrammar').and.resolveTo({ success: true });
        });

        it('logs a warning when already activated', async () => {
            spyOn(gi, 'isActivated').and.returnValue(true);
            const warnSpy = spyOn(console, 'warn');

            await gi.activate();

            expect(warnSpy).toHaveBeenCalledWith(`Tried activating GrammarInteraction '${gi.grammarCtxId}', but interaction is already activate.`);
        });

        it('throws an exception when current activated GrammarInteraction is not in READY state', async () => {
            const gi2 = openRecordingGrammarFactory(sm, ri, sm.eventBus);
            gi2.state = AsrGrammarInteractionState.RECOGNIZING;
            spyOnProperty(tm, 'activeGrammarCtx', 'get').and.returnValue(gi2);

            await expectAsync(gi.activate()).toBeRejectedWithError(`Can't activate GrammarInteraction since the current active GrammarInteraction is not in READY state.`);
        });

        it('should dispatch an ACTIVATING_GRAMMER_SET event', async () => {
            const dispatchSpy = spyOn(gi.eventBus, 'dispatch');

            await gi.activate();

            expect(dispatchSpy).toHaveBeenCalledWith(events.ACTIVATING_GRAMMAR_SET, { grammar: gi });
        });

        it('should call setGrammar on the active transport', async () => {
            await gi.activate();

            expect(setGrammarSpy).toHaveBeenCalledWith(gi, classToPlain(gi.grammar), false);
        });

        it('sets the returned value as lastActivationResult and returns it', async () => {
            const resp = await gi.activate();

            expect(resp).toEqual({ success: true });
            expect(gi.lastActivationResult).toEqual({ success: true });
        });

        describe('when setting grammar failed', () => {

            beforeEach(() => {
                setGrammarSpy.and.resolveTo({ success: false, message: 'failure' });
            });

            it('should dispatch an ACTIVATING_GRAMMAR_SET_FAILED event', async () => {
                const dispatchSpy = spyOn(gi.eventBus, 'dispatch');

                await gi.activate().catch(() => void 0);

                expect(dispatchSpy).toHaveBeenCalledWith(events.ACTIVATING_GRAMMAR_SET_FAILED, { error: 'failure' });
            });

            it('should throw an error', async () => {
                await expectAsync(gi.activate()).toBeRejectedWithError(`Invalid grammar: failure`);
            });

        });

        it('should dispatch an ACTIVATED_GRAMMAR_SET when succesfull', async () => {
            const dispatchSpy = spyOn(gi.eventBus, 'dispatch');

            await gi.activate();

            expect(dispatchSpy).toHaveBeenCalledWith(events.ACTIVATED_GRAMMAR_SET, { grammar: gi });
        });

    });

    describe('#record', () => {
        let activatedSpy: jasmine.Spy;
        let recorderStartSpy: jasmine.Spy;

        beforeEach(() => {
            jasmine.clock().uninstall();
            jasmine.clock().install();

            activatedSpy = spyOn(gi, 'isActivated').and.returnValue(true);
            recorderStartSpy = spyOn(ri, 'start');
        });

        it('throws an error when not activated', () => {
            activatedSpy.and.returnValue(false);

            expect(() => gi.record()).toThrowError(`Can't record since GrammarInteraction is not activated. Make sure to activate this instance first.`);
        });

        it('throws an error when current state is not ready', () => {
            gi.state = AsrGrammarInteractionState.RECOGNIZING;

            expect(() => gi.record()).toThrowError(`GrammarInteraction can't start recording: requires state to be '${AsrGrammarInteractionState.READY}' instead of '${AsrGrammarInteractionState.RECOGNIZING}'.`);
        });

        it('starts recording on the set recorder', () => {
            gi.record();

            expect(recorderStartSpy).toHaveBeenCalledWith(jasmine.any(Function));
        });

        describe('on audio clb', () => {
            let clb: (audio: Blob) => void;
            let sendAudioSpy: jasmine.Spy;

            beforeEach(() => {
                gi.record();
                clb = recorderStartSpy.calls.first().args[0];

                sendAudioSpy = spyOn(tm, 'sendAudio').and.resolveTo();
            });

            it('skips sending audio when transport is disconnected', () => {
                spyOnProperty(tm, 'connected', 'get').and.returnValue(false);

                clb(new Blob([]));

                expect(sendAudioSpy).not.toHaveBeenCalled();
            });

            it('passes on the recorder\'s audio to transport', () => {
                spyOnProperty(tm, 'connected', 'get').and.returnValue(true);
                const blob = new Blob([]);

                clb(blob);

                expect(sendAudioSpy).toHaveBeenCalledWith(blob);
            });

            it('stops recording when sending audio failed', (done) => {
                spyOnProperty(tm, 'connected', 'get').and.returnValue(true);
                sendAudioSpy.and.rejectWith('failure in audio');
                const consoleSpy = spyOn(console, 'error');
                const stopSpy = spyOn(gi, 'stop').and.callFake(() => {
                    expect(stopSpy).toHaveBeenCalled();
                    expect(consoleSpy).toHaveBeenCalledWith('Error while sending audio, stop recording.', 'failure in audio');
                    done();
                    return Promise.resolve(undefined);
                });

                clb(new Blob([]));
            });
        });

        it('sets the state to RECORDING', () => {
            gi.record();

            expect(gi.state).toEqual(AsrGrammarInteractionState.RECORDING);
        });

        it('automatically stops recording after 60 seconds', async () => {
            const stopSpy = spyOn(gi, 'stop');

            gi.record();

            jasmine.clock().tick(50000);
            expect(gi.state).toEqual(AsrGrammarInteractionState.RECORDING);

            jasmine.clock().tick(15000);
            expect(stopSpy).toHaveBeenCalled();
        });

        it('sets minRecordingTimeReached to false', async () => {
            gi.record();

            expect((gi as any).minRecordingTimeReached).toBeFalse();
        });

        it('sets minRecordingTimeReached to true after 500ms', () => {
            gi.record();
            jasmine.clock().tick(500);

            expect((gi as any).minRecordingTimeReached).toBeTrue();
        });

        it('stops recording when minRecordingTimeReached is true, useEOS is true and an EOS event is emitted', () => {
            const stopSpy = spyOn(gi, 'stop');

            gi.record(true);

            jasmine.clock().tick(500);

            expect(stopSpy).not.toHaveBeenCalled();

            gi.eventBus.dispatch(events.EOS);

            expect(stopSpy).toHaveBeenCalled();
        });

    });

    describe('#stop', () => {
        let recorderStopSpy: jasmine.Spy;
        let clearSpy: jasmine.Spy;
        let stateSpy: jasmine.Spy;

        beforeEach(() => {
            recorderStopSpy = spyOn(ri, 'stop');
            clearSpy = spyOn(window, 'clearTimeout');

            stateSpy = spyOnProperty(gi, 'state', 'get').and.returnValue(AsrGrammarInteractionState.RECORDING);

            (gi as any).autoStopTimeout = 10;
            (gi as any).minTimeTimeout = 20;
            (gi as any).EOSEventHandler = () => void 0;
        });

        it('logs a warning when not in RECORDING state', () => {
            stateSpy.and.returnValue(AsrGrammarInteractionState.READY);
            const logSpy = spyOn(console, 'warn');

            gi.stop();

            expect(logSpy).toHaveBeenCalledWith(`stop() was called but current recording state is not '${AsrGrammarInteractionState.RECORDING}'`);
        });

        it('clears the autoStopTimeout', () => {
            gi.stop();

            expect(clearSpy).toHaveBeenCalledWith(10);
            expect((gi as any).autoStopTimeout).toBeUndefined();
        });

        it('clears the minTimeTimeout', () => {
            gi.stop();

            expect(clearSpy).toHaveBeenCalledWith(20);
            expect((gi as any).minTimeTimeout).toBeUndefined();
        });

        it('removes the EOSEventHandler from the eventbus', () => {
            const removeEventSpy = spyOn(gi.eventBus, 'removeEventListener');
            const fn = (gi as any).EOSEventHandler;

            gi.stop();

            expect(removeEventSpy).toHaveBeenCalledWith(events.EOS, fn);
        });

        it('stops the recorder', () => {
            gi.stop();

            expect(recorderStopSpy).toHaveBeenCalled();
        });

        it('changes the state to recognizing when minRecordingTimeReached is true', () => {
            const stateSetSpy = spyOnProperty(gi, 'state', 'set');

            (gi as any).minRecordingTimeReached = true;
            gi.stop();

            expect(stateSetSpy).toHaveBeenCalledWith(AsrGrammarInteractionState.RECOGNIZING);
        });

        it('calls transport.waitForRecognitionResult when minRecordingTimeReached is true', () => {
            (gi as any).minRecordingTimeReached = true;
            const waitSpy = spyOn((gi as any).session.transport, 'waitForRecognitionResult');

            gi.stop();

            expect(waitSpy).toHaveBeenCalled();
        });

        it('does not initialize an AsrResult when when minRecordingTimeReached and skipRecognition are true', () => {
            const asrConstructor = spyOn(AsrResult, 'initialize');

            (gi as any).minRecordingTimeReached = true;
            gi.stop(true);

            expect(asrConstructor).not.toHaveBeenCalled();
        });

        it('changes the state to ready when minRecordingTimeReached is false', () => {
            const stateSetSpy = spyOnProperty(gi, 'state', 'set');

            (gi as any).minRecordingTimeReached = false;
            gi.stop();

            expect(stateSetSpy).toHaveBeenCalledWith(AsrGrammarInteractionState.READY);
        });

        it('emits a value to result$ when minRecordingTimeReached is true', async () => {
            const resultProm = gi.result$.pipe(first()).toPromise();
            spyOn((gi as any).session.transport, 'waitForRecognitionResult').and.resolveTo({});

            (gi as any).minRecordingTimeReached = true;
            await gi.stop();

            expectAsync(resultProm).toBeResolvedTo(jasmine.any(AsrResult));
        });

    });

    describe('isRecording()', () => {

        it('returns true when state is recording', () => {
            spyOnProperty(gi, 'state', 'get').and.returnValue(AsrGrammarInteractionState.RECORDING);

            expect(gi.isRecording()).toBeTrue();
        });

    });

    describe('isReady', () => {

        it('returns true when state is ready', () => {
            spyOnProperty(gi, 'state', 'get').and.returnValue(AsrGrammarInteractionState.READY);

            expect(gi.isReady()).toBeTrue();
        });

    });

    describe('isRecognizing()', () => {

        it('returns true when state is recognizing', () => {
            spyOnProperty(gi, 'state', 'get').and.returnValue(AsrGrammarInteractionState.RECOGNIZING);

            expect(gi.isRecognizing()).toBeTrue();
        });

    });

});
