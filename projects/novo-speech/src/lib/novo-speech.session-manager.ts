import { fromEvent, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';
import { ConnectionQualityChangedEvent, events, SessionStateChangedEvent, TransportLostConnectionEvent } from './events/events.constants';
import { EventBus } from './events/novo-speech.event-bus';
import { LanguageCode } from './models';
import { SESSION_SETUP_TIME, SESSION_TIMEOUT } from './models/constants';
import { NovoSpraakError, NovoSpraakErrorCode } from './models/errors';
import { AsrSession } from './models/session';
import { AsrConnectionQuality } from './models/types';
import { Json } from './models/utilities';
import { AsrSessionState } from './novo-speech.interfaces';
import { NovoSpeechTransport } from './novo-speech.transport';
import { CodecType } from './recorder/util/recorder.interface';

/**
 * Storage Key used when storing the Context ID
 */
export const CTX_ID_STORAGE_KEY = 'novoSpraakCtxId';

export interface GetSessionParams {
    l2: string;
    local: boolean;
    snodeid?: number;
    context: string;
}

export interface NovoSpraakRequestResult<T = {} | undefined> {
    data: T;
    status: string;
    headers: { location?: string };
}

/**
 * A SessionManager instance is responsible for setting up and maintaining an
 * active session with the remote API, as well as the actual transport connection
 * used for bi-directional communication.
 */
export class SessionManager {
    local = false;

    /**
     * Current ASR Session information
     */
    activeSession?: AsrSession;

    /**
     * Timestamp of last message received within this session
     */
    lastMessageDate?: Date;

    /**
     * Event bus (channel) used for Session Manager
     */
    readonly eventBus: EventBus;


    // tslint:disable-next-line: variable-name
    private _internalState: AsrSessionState = AsrSessionState.NoSession;

    set state(state: AsrSessionState) {
        if (this._internalState === state) { return; }
        this.eventBus.dispatch<SessionStateChangedEvent>(events.SESSION_STATE_CHANGED, { ctx: this.ctxId, previous: this._internalState, state });
        this._internalState = state;
    }
    get state(): AsrSessionState {
        return this._internalState;
    }

    transport?: NovoSpeechTransport;

    /**
     * Unique identifier for SessionManager instance
     */
    readonly ctxId: string = uuid();

    /**
     * Observalbe emitting controller state changes
     */
    readonly state$: Observable<AsrSessionState>;

    /**
     * Emits current connection quality changes
     */
    readonly connectionQuality$: Observable<AsrConnectionQuality>;

    /**
     * Current context identifier
     */
    readonly currentContextId: string = this.getContextId();

    /**
     * Default headers send with every remote 'call'
     */
    private headers: { [key: string]: string } = { 'Content-Type': 'application/json' };

    /**
     * Controller used to abort pending 'fetch' requests
     */
    private abortCtrl = typeof AbortController !== 'undefined' ? new AbortController() : undefined;

    /**
     * Contains a promise when a new session is being established.
     * The promise resolves as soon as the new session has been established.
     */
    private pendingSessionPromise?: Promise<NovoSpeechTransport>;

    constructor(
        token: string,
        readonly api: string = 'https://gm.novo-learning.com/v0',
        readonly targetLanguage: LanguageCode,
        private readonly codec: CodecType | undefined,
        readonly sNodeId?: number,
        // tslint:disable-next-line: variable-name
        _eventBus: EventBus = new EventBus(),
    ) {
        this.setAuthenticationToken(token);
        this.eventBus = _eventBus.channel(`SM${this.ctxId}`);

        // Initialize state$ and connectionQuality$ with newly created event channel
        this.state$ = fromEvent<SessionStateChangedEvent>(
            this.eventBus,
            events.SESSION_STATE_CHANGED
        ).pipe(
            filter(({ ctx }) => ctx === this.ctxId),
            map(({ state }) => state)
        );
        this.connectionQuality$ = fromEvent<ConnectionQualityChangedEvent>(
            this.eventBus,
            events.CONNECTION_QUALITY_CHANGED
        ).pipe(
            map(({ quality }) => quality),
        );

        // Setup event listeners and handler
        this.eventBus.addEventListener(events.TRANSPORT_MESSAGE_RECEIVED, this.messageReceived.bind(this));
        this.eventBus.addEventListener<TransportLostConnectionEvent>(events.TRANSPORT_LOST_CONNECTION, ({ code }) => {
            this.invalidateSession();
            // Code 1001 means we lost a connection on purpose, any other code is probably accidental
            // Code 4000 means we invalidated the session
            if (code !== 1001 && code !== 4000) {
                this.establish().catch(console.error);
            }
        });
    }

    /**
     * Checks if the current AsrSession is still valid
     */
    isValid() {
        if (this.activeSession == null) {
            return false;
        }
        if (this.transport == null) {
            return false;
        }
        if (this.activeSession.language !== this.targetLanguage) {
            return false;
        }
        if (this.activeSession.snode?.id !== this.sNodeId) {
            return false;
        }
        if (this.lastMessageDate != null) {
            const msAgo: number = Math.abs(new Date().getTime() - this.lastMessageDate.getTime());
            return msAgo < SESSION_TIMEOUT;
        }
        return true;
    }

    /**
     * Check if the current transport connection is active.
     * @param force (optional)  When true force a new connection check, otherwise wait for an existing check
     *                          Use force true when applications visibility is set to foreground
     */
    async checkConnection(force?: boolean): Promise<void> {
        // When transport is undefined, reconnect
        // This should solve 'Provided SessionManager has no active TransportConnection' exceptions
        if (!this.transport) {
            return (await this.establish()).checkConnection(force);
        }
        return this.transport?.checkConnection(force) || Promise.resolve();
    }

    /**
     * Invalidate existing session.
     *
     * Currently this just clears the locally stored session. In the future
     * this might should also invalidate the remote session.
     */
    private async invalidateSession(): Promise<void> {
        this.activeSession = undefined;

        this.transport?.clear();
        this.transport = undefined;

        this.state = AsrSessionState.NoSession;
    }

    /**
     * Check if there's a valid session set
     */
    hasValidSession(): boolean {
        return this.activeSession != null;
    }

    /**
     * Establishes a new session and transport connection, or verifies the
     * active connection.
     *
     * This will return the current session when available and still valid.
     * If this is not the case it will abort all pending requests and create
     * a new session.
     */
    async establish(): Promise<NovoSpeechTransport> {
        if (this.pendingSessionPromise != null) {
            return this.pendingSessionPromise;
        }

        if (this.isValid()) {
            return this.transport as NovoSpeechTransport;
        }

        // invalidate current session abort all pending calls
        this.pendingSessionPromise = this.invalidateSession().then(() => {
            this.abortAllRequests();
            // Invalidate session should be called before changing the state to 'Starting'
            this.state = AsrSessionState.StartingSession;

            const params: GetSessionParams = {
                l2: this.targetLanguage,
                local: this.local,
                context: this.currentContextId,
                snodeid: this.sNodeId ?? undefined
            };

            return new Promise<{ error?: string } & Json<AsrSession>>((resolve, reject) => {

                this.call<{ error?: string } & Json<AsrSession>>('/sessions', params).then(result => {
                    if (result == null) { reject(undefined); return; }

                    if (Object.keys(result.data).length > 0) {
                        resolve(result.data);
                    } else {
                        if (result.headers.location == null) {
                            reject(new Error('Expected to receive a Location to poll, but none was given'));
                        }
                        setTimeout(() => {
                            this.pollSession(result.headers.location as string, resolve, reject);
                        }, SESSION_SETUP_TIME);
                    }
                }, error => reject(error));
            });
        }).then(session => this.handleNewSession(session, this.codec)
        ).finally(this.resetPendingPromise.bind(this));

        return this.pendingSessionPromise;
    }

    /**
     * Handles a new session response by initializing the model
     * and establishing a transport connection
     */
    async handleNewSession(session: null | { error?: string } & Json<AsrSession>, codec?: CodecType): Promise<NovoSpeechTransport> {
        if (session == null || session.error) {
            this.state = AsrSessionState.StartingSessionFailed;
            throw new Error(`No session. Error: ${session && session.error || ''}`);
        }

        const asrSession = AsrSession.initialize(session, this.targetLanguage);
        this.activeSession = asrSession;
        this.transport = await this.setupTransportConnection(asrSession.proxyURL);

        if (codec) {
            await this.transport.initCodec(codec);
        }

        this.state = AsrSessionState.StartedSession;

        return this.transport;
    }

    /**
     * Reset pending promise meta
     */
    private resetPendingPromise() {
        this.pendingSessionPromise = undefined;
    }

    /**
     * End the current session.
     *
     * Clear the current property and invalidate the remote session.
     */
    async end(): Promise<NovoSpraakRequestResult | void> {
        if (this.activeSession == null) {
            return Promise.resolve();
        }

        this.abortAllRequests(); // abort all pending calls

        const result = await this.call(`/sessions/${this.activeSession.id}`, {}, 'DELETE').catch(err => { });
        this.invalidateSession();

        return result;
    }


    /**
     * Setup a new websocket for the given Proxy
     */
    private async setupTransportConnection(proxyUrl: string): Promise<NovoSpeechTransport> {
        const ws = new NovoSpeechTransport(proxyUrl, this.eventBus);

        console.log('Connect to WS...', new Date());
        await ws.connect();
        console.log('Websocket connected:', new Date());

        return ws;
    }

    /**
     * Update authentication token header sent with every call.
     */
    setAuthenticationToken(token: string): void {
        this.headers['Authentication-Token'] = token;
    }

    /**
     * Update last message received date
     */
    private messageReceived(): void {
        this.lastMessageDate = new Date();
    }

    /**
     * Initialize a new context ID.
     *
     * Depending on the user's device this ID should be either stored
     * in the device's Local Storage (Cordova App Webview) or
     * Session Storage (Browser allowing multiple tabs).
     */
    private getContextId(): string {
        if(typeof window === "undefined") {
            return uuid();
        }

        const storage = 'cordova' in window && window.cordova != null ? localStorage : sessionStorage;
        let contextId = storage.getItem(CTX_ID_STORAGE_KEY);

        if (contextId == null) {
            contextId = uuid();
            try {
                storage.setItem(CTX_ID_STORAGE_KEY, contextId);
            } catch (err) {
                // Could not store contextId. This wont break the library
                // but should be noticed.
                console.error(`Failed to store context ID: ${err}`);
            }
        }

        return contextId;
    }

    /***
     * Cancel all pending requests and initialize a new AbortController
     */
    private abortAllRequests() {
        if(this.abortCtrl) {
            this.abortCtrl.abort();
            this.abortCtrl = new AbortController();
        }
    }

    /**
     * Serialize the given input into a NovoSpraakError
     */
    private constructError(responseJson: any, statusCode?: number, statusText?: string): NovoSpraakError {
        let errorCode: NovoSpraakErrorCode = NovoSpraakErrorCode.UnknownError;
        let errorMsg = 'Unknown error';
        if (responseJson != null && typeof responseJson === 'object' && responseJson.code != null) {
            switch (responseJson.code) {
                case 1101:
                case 1201:
                case 1301:
                case 1401:
                    errorCode = NovoSpraakErrorCode.NotAuthorized;
                    break;
                case 1102:
                case 1202:
                    errorCode = NovoSpraakErrorCode.NotOwner;
                    break;
                case 1103:
                case 1203:
                    errorCode = NovoSpraakErrorCode.SessionNotFound;
                    break;
                case 1405:
                    errorCode = NovoSpraakErrorCode.TooManyConcurrentSessions;
                    break;
            }

            errorMsg = responseJson.message;
        } else {
            errorMsg = statusText ?? errorMsg;

            switch (statusCode) {
                case 403:
                    errorCode = NovoSpraakErrorCode.NotAuthorized;
                    break;
                case 404:
                    errorCode = NovoSpraakErrorCode.SessionNotFound;
                    break;
                default:
            }
        }

        return new NovoSpraakError(errorMsg, errorCode);
    }

    /**
     * Make a remote call.
     *
     * Note that pending requests will be cancalled when @see {abortAllRequests}
     * is called.
     *
     * By default 'resource' should be a relative path. Make sure to set @param {includeBase}
     * as false when providing a full URL.
     */
    private async call<T = {}>(
        resource: string,
        params: GetSessionParams | {} | null,
        method: 'POST' | 'DELETE' | 'GET' = 'POST',
        includeBase = true
    ): Promise<NovoSpraakRequestResult<T> | undefined> {
        if (this.headers['Authentication-Token'] == null) {
            throw new NovoSpraakError(
                'No authentication token set',
                NovoSpraakErrorCode.NotAuthorized,
            );
        }

        // Optionally include API URL base
        resource = includeBase ? this.api + resource : resource;

        // Add params as QueryString for GET operations
        if (method === 'GET' && params != null) {
            resource += `?${Object.keys(params).map((k) => `${k}=${(params as any)[k]}`).join('&')}`;
        }

        const resp = await fetch(resource, {
            method,
            headers: this.headers,
            body: params && method !== 'GET' ? JSON.stringify(params) : null,
            signal: this.abortCtrl?.signal
        }).catch(err => {
            // A DOMException is raised when a request is aborted. This happens when invalidateSession is called.
            if (err instanceof DOMException && err.code === DOMException.ABORT_ERR) { return; }
            return { status: 0, statusText: 'Could not contact speech backend', ok: false, json: () => Promise.resolve({}), headers: new Map() };
        });
        if (resp == null) { return; }

        // Try to retrieve JSON data before we actually know if the request was successful.
        const data = await resp.json().catch(_ => null);
        if (!resp.ok) {
            throw this.constructError(data, resp.status, resp.statusText);
        }

        return {
            data,
            status: resp.statusText,
            headers: {
                location: resp.headers.get('Location') || 'Unknown'
            }
        };
    }

    /**
     * Poll session and resolve when it's ready
     */
    private pollSession<T>(url: string, resolve: (value: T) => void, reject: (reason?: NovoSpraakError) => void): void {
        this.call<{ result?: any[] }>(url, null, 'GET', false).then(resp => {
            if (resp) {
                const { data } = resp;
                if (data && typeof data === 'object' && data.result != null) {
                    resolve(data.result[0]);
                    return;
                }
            }
            setTimeout(() => this.pollSession(url, resolve, reject), 1000);
        }).catch(err => reject(err));
    }
}
