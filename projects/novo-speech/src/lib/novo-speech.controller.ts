import { events } from './events/events.constants';
import { EventBus } from './events/novo-speech.event-bus';
import { AsrGrammar, LanguageCode } from './models';
import { RecorderApi } from './models/constants';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import { forcedAlignmentGrammarFactory, multipleChoiceGrammarFactory, openRecordingGrammarFactory } from './novo-speech.grammar-interaction-factory';
import { NovoSpeechConfig } from './novo-speech.interfaces';
import { SessionManager } from './novo-speech.session-manager';
import { CordovaRecorder } from './recorder/cordova-recorder';
import { MediaRecorderWrapper } from './recorder/media-recorder-wrapper.recorder';
import { CodecType, RecorderInterface } from './recorder/util/recorder.interface';
import { WebAudioRecorder } from './recorder/web-audio.recorder';

/**
 * Use the NovoSpeechController within your application to
 * manage Sessions and generate GrammarInteractions.
 *
 * Although it's possible to have multiple instances of the Controller
 * within your application is recommended to use it as a Singleton.
 *
 * The Novo Speech library is built around @see {GrammarInteraction} instances
 * which need to be intialized with a @see {SessionManager}. Each SessionManager
 * instance controls a session with the Novo Speech API based on the request
 * Language and optionally sNodeID.
 *
 * Sessions with the same Language and sNodeID are automatically re-used, preventing
 * your application from hitting the active-sessions ceiling which is currently
 * (05-2020) set at 5 active sessions per user/location.
 *
 * See the README.md or @see {ensureSession()} method for more information.
 */
export class NovoSpeechController {

    /**
     * Holds the sessionManager responsible for maintainig
     * an active session.
     */
    readonly sessionManagerFactory: (language: LanguageCode, codec: CodecType | undefined, sNodeId?: number) => SessionManager;

    /**
     * Active SessionManager instances
     */
    readonly activeSessions: { [key: string]: SessionManager } = {};

    /**
     * Choosen recorder API
     */
    recorder: RecorderInterface | undefined;

    /**
     * Main EventBus for inter-instance communication and state changes
     */
    readonly eventBus = new EventBus();

    constructor(
        readonly novoSpeechConfig: NovoSpeechConfig
    ) {
        this.sessionManagerFactory = (language: LanguageCode, codec: CodecType | undefined, sNodeId?: number) => {
            if (!novoSpeechConfig.token) {
                throw new Error('No User Authentication Token is set');
            }
            return new SessionManager(novoSpeechConfig.token, novoSpeechConfig.api, language, codec, sNodeId, this.eventBus);
        };
    }

    /**
     * Ensures an active session for the given language exists.
     *
     * Note that a NovoSpeechController instance only handles one active session
     * at the same time.
     */
    async ensureSession(language: LanguageCode, sNodeId?: number): Promise<SessionManager> {
        const recorder = await this.getRecorder();

        // Get an existing or new SessionManager instance
        const session = this.getSessionInstance(language, recorder?.codec || undefined, sNodeId);
        // Setup connection and activate recorder codec
        await session.establish();

        return session;
    }

    /**
     * Creates a new GrammarInteraction instance which can be used
     * to perform ForcedAlignment Grammar interactions
     */
    async getForcedAlignmentGrammar(session: SessionManager, text: string | string[], returnIntermediateResults = false): Promise<NovoSpeechGrammarInteraction> {
        if (!session.transport) {
            throw new Error(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        }
        const recorder = await this.getRecorder();
        return this.activateGrammarSet(
            forcedAlignmentGrammarFactory(
                text,
                returnIntermediateResults,
                session.targetLanguage,
                session,
                recorder,
                this.eventBus,
            )
        );
    }

    /**
     * Creates a new GrammarInteraction instance which can be used
     * to perform MultipleChoise Grammar interactions
     */
    async getMultipleChoiceGrammar(session: SessionManager, options: string[], returnIntermediateResults = false): Promise<NovoSpeechGrammarInteraction> {
        if (!session.transport) {
            throw new Error(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        }
        const recorder = await this.getRecorder();
        return this.activateGrammarSet(
            multipleChoiceGrammarFactory(
                options,
                returnIntermediateResults,
                session.targetLanguage,
                session,
                recorder,
                this.eventBus,
            )
        );
    }

    /**
     * Creates a new GrammarInteraction instance which can be used
     * to perform ForcedAlignment Grammar interactions
     */
    async getOpenRecordingGrammar(session: SessionManager): Promise<NovoSpeechGrammarInteraction> {
        if (!session.transport) {
            throw new Error(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        }
        const recorder = await this.getRecorder();
        return this.activateGrammarSet(
            openRecordingGrammarFactory(
                session,
                recorder,
                this.eventBus
            )
        );
    }

    /**
     * Creates a new GrammarInteraction instance with
     * the provided AsrGrammer.
     *
     * This should only be used in low-level implementations.
     * Prefer the usage of other Grammar initializer methods.
     */
    async getCustomGrammar(session: SessionManager, grammar: AsrGrammar, returnIntermediateResults = false): Promise<NovoSpeechGrammarInteraction> {
        if (!session.transport) {
            throw new Error(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        }
        const recorder = await this.getRecorder();
        return this.activateGrammarSet(
            new NovoSpeechGrammarInteraction(grammar, returnIntermediateResults, session, recorder, this.eventBus)
        );
    }

    /**
     * Remove session so a new session will be retrieved when calling start/ensure session
     */
    async invalidateSession(targetLanguage: LanguageCode, sNodeId?: number): Promise<void>;
    async invalidateSession(session: SessionManager): Promise<void>;
    async invalidateSession(smOrLanguage: SessionManager | LanguageCode, sNodeId?: number): Promise<void> {
        const sessionManager = (smOrLanguage instanceof SessionManager) ? smOrLanguage : this.getSessionInstance(smOrLanguage, undefined, sNodeId, false);
        if (!sessionManager) {
            console.warn(`No active SessionManager found, skipping...`);
            return;
        }

        // End sessions
        await sessionManager.end();

        // Remove instance from local list of sessions
        for (const id of Object.keys(this.activeSessions)) {
            if (this.activeSessions[id] === sessionManager) {
                delete this.activeSessions[id];
            }
        }
    }

    private async getRecorder(): Promise<RecorderInterface | undefined> {
        if (!this.recorder && this.novoSpeechConfig.recorder !== RecorderApi.NONE) {
            const recorderApi = this.chooseRecorderApi(this.novoSpeechConfig.recorder);
            this.recorder = await this.setupRecorderApi(recorderApi);
        }
        return this.recorder;
    }

    private async activateGrammarSet<T extends NovoSpeechGrammarInteraction>(grammarSet: T): Promise<T> {
        await grammarSet.activate();
        return grammarSet;
    }

    /**
     * Returns a session instance for the requested language and snode. This will either
     * re-use an existing SessionManager, or create a new instance when none exists yet.
     */
    private getSessionInstance(targetLanguage: LanguageCode, codec: CodecType | undefined, sNodeId?: number): SessionManager;
    private getSessionInstance(targetLanguage: LanguageCode, codec: CodecType | undefined, sNodeId: number | undefined, autoCreate: false): SessionManager | undefined;
    private getSessionInstance(targetLanguage: LanguageCode, codec: CodecType | undefined, sNodeId?: number, autoCreate?: false): SessionManager | undefined {
        const id = `${targetLanguage}.${sNodeId || '-'}`;
        if (!this.activeSessions[id] && autoCreate !== false) {
            this.activeSessions[id] = this.sessionManagerFactory(targetLanguage, codec, sNodeId);
        }

        return this.activeSessions[id];
    }

    /**
     * Setup and resolve an instance of the provided RecorderApi type
     */
    async setupRecorderApi(recorderApi?: RecorderApi): Promise<RecorderInterface | undefined> {
        let recorder: RecorderInterface;

        // This library supports integration with Cordova which means
        // we'll always need to request microphone permissions, even when
        // we're not using the CordovaRecorder
        if (window.cordova != null) {
            await CordovaRecorder.ensurePermissions();
        }

        switch (recorderApi) {
            case RecorderApi.NONE:
                return undefined;
            case RecorderApi.CORDOVA:
                recorder = new CordovaRecorder();
                break;
            case RecorderApi.HTML5_WEBAUDIO:
                await WebAudioRecorder.ensurePermissions();
                recorder = new WebAudioRecorder(() => this.eventBus.dispatch(events.MIC_CLIPPING));
                break;
            case RecorderApi.HTML5_MEDIARECORDER:
                await MediaRecorderWrapper.ensurePermissions();
                recorder = new MediaRecorderWrapper();
                break;
            default:
                throw new Error(`Invalid RecorderApi ${recorderApi}`);
        }
        return recorder;
    }

    /**
     * Choose the best RecorderApi to use based on the user's environment
     */
    chooseRecorderApi(forceRecorderApi?: RecorderApi): RecorderApi | undefined {
        if (forceRecorderApi != null) {
            return forceRecorderApi;
        }

        if ('cordova' in window) {
            const device = window.device?.platform || 'unknown';
            if (device?.toLowerCase() === 'ios') {
                console.log('Audio Recorder: Use Cordova audioinput plugin (only for iOS!)');
                return RecorderApi.CORDOVA;
            }
        }

        if (
            navigator.mediaDevices &&
            navigator.mediaDevices.getUserMedia &&
            'MediaRecorder' in window
        ) {
            console.log('Audio Recorder: Use MediaRecorder API');
            return RecorderApi.HTML5_MEDIARECORDER;
        } else if (
            navigator.getUserMedia ||
            'webkitGetUserMedia' in navigator ||
            'mozGetUserMedia' in navigator ||
            'msGetUserMedia' in navigator
        ) {
            // Use HTML5 recorder is getUserMedia is supported
            console.log('Audio Recorder: Use WebAudio API');
            return RecorderApi.HTML5_WEBAUDIO;
        }
    }

    /**
     * Update the current User Authentication Token by a new one.
     *
     * By default this will not update the existing SessionManagers. Set
     * 'updateExisting' to true if you want to update the existing SessionManagers.
     */
    updateToken(token: string, updateExisting?: boolean): void {
        this.novoSpeechConfig.token = token;
        if (updateExisting) {
            for (const session in this.activeSessions) {
                if (this.activeSessions[session] instanceof SessionManager) {
                    this.activeSessions[session].setAuthenticationToken(token);
                }
            }
        }
    }
}
