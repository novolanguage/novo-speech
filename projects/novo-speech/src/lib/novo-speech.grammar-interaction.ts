import { classToPlain } from 'class-transformer';
import { fromEvent, Observable, ReplaySubject } from 'rxjs';
import { filter, map, shareReplay, startWith } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';
import { ActiveGrammarInteractionNotReady } from './errors/active-grammar-interaction-not-ready.error';
import { GrammarInteractionNotActive } from './errors/grammar-interaction-not-active.error';
import { ActivatedGrammarSetEvent, ActivatingGrammarSetEvent, ActivatingGrammarSetFailedEvent, events, GrammarInteractionStateChangedEvent, IntermediateResultEvent } from './events/events.constants';
import { EventBus } from './events/novo-speech.event-bus';
import { AsrIntermediateResult, AsrResult } from './models';
import { MAX_RECORDING_TIME, MIN_RECORDING_TIME } from './models/constants';
import { AsrGrammar } from './models/grammar';
import { AsrGrammarInteractionState } from './novo-speech.interfaces';
import { SessionManager } from './novo-speech.session-manager';
import { RecorderInterface } from './recorder/util/recorder.interface';

export interface GrammarResult {
    message: string;
    success: boolean;
}

/**
 * Use the NovoSpeechGrammarInteraction instance to interact
 * with your set grammar.
 *
 * Note that each session can only maintain one active GrammarInteraction.
 * If you're using multiple GrammarInteraction instances you'll have
 * to make sure to activate the grammar set before using it.
 */
export class NovoSpeechGrammarInteraction {

    /**
     * Emits results as soon as recording ends
     */
    readonly result$: ReplaySubject<AsrResult> = new ReplaySubject<AsrResult>(1);
    /**
     * Broadcast intermediate results during a recording
     */
    readonly intermediateResults$: Observable<AsrIntermediateResult> = fromEvent<IntermediateResultEvent>(this.eventBus, events.INTERMEDIATE_RESULT).pipe(
        filter(({ ctx }) => ctx === this.grammarCtxId),
        map(({ result }) => result)
    );

    /**
     * Unique identifier which is generated for each
     * Grammar Set and used to identify when the active set
     * changes.
     */
    readonly grammarCtxId: string = uuid();

    /**
     * Contains the response received from the last activation
     */
    lastActivationResult: any;

    // tslint:disable-next-line: variable-name
    private _internalState: AsrGrammarInteractionState = AsrGrammarInteractionState.READY;

    get state(): AsrGrammarInteractionState {
        return this._internalState;
    }
    set state(state: AsrGrammarInteractionState) {
        if (this._internalState === state) { return; }
        this.eventBus.dispatch<GrammarInteractionStateChangedEvent>(events.GRAMMAR_INTERACTION_STATE_CHANGED, {
            ctx: this.grammarCtxId,
            state,
            previous: this._internalState
        });
        this._internalState = state;
    }

    readonly state$: Observable<AsrGrammarInteractionState> = fromEvent<GrammarInteractionStateChangedEvent>(this.eventBus, events.GRAMMAR_INTERACTION_STATE_CHANGED).pipe(
        filter(({ ctx }) => ctx === this.grammarCtxId),
        map(e => e.state),
        startWith(this.state),
        shareReplay(1)
    );

    private autoStopTimeout?: number;
    private minTimeTimeout?: number;
    private EOSEventHandler?: () => void;
    private minRecordingTimeReached?: boolean;

    constructor(
        readonly grammar: AsrGrammar,
        readonly returnIntermediateResults = false,
        private readonly session: SessionManager,
        private readonly recorder: RecorderInterface | undefined,
        readonly eventBus: EventBus,
    ) {

        this.eventBus.addEventListener(events.TRANSPORT_LOST_CONNECTION, () => {
            if (this.isRecording()) {
                this.stop(true);
            }
        });
    }

    /**
     * Activate the current grammar set
     */
    async activate(): Promise<any> {
        if (this.isActivated()) {
            console.warn(`Tried activating GrammarInteraction '${this.grammarCtxId}', but interaction is already activate.`);
            return;
        }

        if (!this.canActivate()) {
            throw new ActiveGrammarInteractionNotReady();
        }

        // Set Grammar
        this.eventBus.dispatch<ActivatingGrammarSetEvent>(events.ACTIVATING_GRAMMAR_SET, { grammar: this });

        const jsonGrammar = classToPlain(this.grammar);
        const grammarResult = await this.session.transport?.setGrammar(this, jsonGrammar, this.returnIntermediateResults);

        if (!grammarResult.success) {
            this.eventBus.dispatch<ActivatingGrammarSetFailedEvent>(events.ACTIVATING_GRAMMAR_SET_FAILED, { error: grammarResult.message });
            throw new Error(`Invalid grammar: ${grammarResult.message}`);
        }

        this.eventBus.dispatch<ActivatedGrammarSetEvent>(events.ACTIVATED_GRAMMAR_SET, { grammar: this });

        this.lastActivationResult = grammarResult;
        return grammarResult;
    }

    /**
     * Returns if the current GrammarInteraction can be activated
     */
    canActivate(): boolean {
        return (this.session.transport?.activeGrammarCtx === null || this.session.transport?.activeGrammarCtx?.state === AsrGrammarInteractionState.READY);
    }

    /**
     * Returns if the WebSocket instance has this GrammarSet as
     * 'set' grammarset.
     */
    isActivated(): boolean {
        return this.session.transport?.activeGrammarCtx === this;
    }

    /**
     * Start recording
     */
    record(useEOS = true) {
        if (this.recorder == null) {
            throw Error('Can\'t record as no recorder is set');
        }

        if (!this.isActivated()) {
            throw new GrammarInteractionNotActive();
        }

        if (this.state !== AsrGrammarInteractionState.READY) {
            throw new Error(`GrammarInteraction can't start recording: requires state to be '${AsrGrammarInteractionState.READY}' instead of '${this.state}'.`);
        }

        this.recorder.start(audio => {
            if (this.session.transport?.connected) {
                this.session.transport?.sendAudio(audio).catch(err => {
                    console.error('Error while sending audio, stop recording.', err);
                    this.stop();
                });
            }
        });
        this.state = AsrGrammarInteractionState.RECORDING;

        this.autoStopTimeout = window.setTimeout(() => {
            if (this.state === AsrGrammarInteractionState.RECORDING) {
                this.stop();
            }
        }, MAX_RECORDING_TIME);

        // Setup timer which triggers as soon as the MIN_RECORDING_TIME is reached
        this.minRecordingTimeReached = false;
        this.minTimeTimeout = window.setTimeout(() => {
            this.minRecordingTimeReached = true;

            // Handle EOS after minimal time was reached
            if (useEOS) {
                this.EOSEventHandler = () => this.stop();
                this.eventBus.addEventListener(events.EOS, this.EOSEventHandler);
            }
        }, MIN_RECORDING_TIME);
    }

    /**
     * Stop recording.
     */
    async stop(skipRecognition?: boolean): Promise<AsrResult | undefined> {
        if(this.recorder == null) {
            return;
        }
        
        if (this.state !== AsrGrammarInteractionState.RECORDING) {
            console.warn(`stop() was called but current recording state is not '${AsrGrammarInteractionState.RECORDING}'`);
            return;
        }

        // Clear timers
        clearTimeout(this.autoStopTimeout);
        clearTimeout(this.minTimeTimeout);
        this.autoStopTimeout = undefined;
        this.minTimeTimeout = undefined;

        // Clear event listeners
        this.eventBus.removeEventListener(events.EOS, this.EOSEventHandler);
        this.EOSEventHandler = undefined;

        // Stop recorder
        this.recorder.stop();

        let result: AsrResult | undefined;

        // Start recognizing when minRecordingTime is reached
        if (this.minRecordingTimeReached) {
            this.state = AsrGrammarInteractionState.RECOGNIZING;

            // Get the result;
            const rawResult = await this.session.transport?.waitForRecognitionResult().catch(console.error);
            // only emit result when skipRecognition isn't false
            if (!skipRecognition && rawResult != null) {
                result = AsrResult.initialize(rawResult);
                this.result$.next(result);
            }
        }

        this.state = AsrGrammarInteractionState.READY;
        return result;
    }

    isRecording(): boolean {
        return this.state === AsrGrammarInteractionState.RECORDING;
    }

    isReady(): boolean {
        return this.state === AsrGrammarInteractionState.READY;
    }

    isRecognizing(): boolean {
        return this.state === AsrGrammarInteractionState.RECOGNIZING;
    }

}
