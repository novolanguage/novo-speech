import { Observable } from 'rxjs';
import { AsrIntermediateResult, AsrResult, RecorderApi } from './models';

export interface NovoSpeechConfig {
    /**
     * Full URL to the API endpoint
     */
    api?: string;

    /**
     * Token to use with API
     */
    token?: string;

    /**
     * Force the recorder which should be used. By default
     * the library will pick the best available recorder interface.
     */
    recorder?: RecorderApi;
}

export enum AsrSessionState {
    NoSession = 'noSession', // No ASR session has been started
    StartingSession = 'startingSession', // Starting ASR session
    StartedSession = 'startedSession', // Started ASR Session
    StartingSessionFailed = 'startingSessionFailed' // State when establishing a session failed
}

export enum AsrTransportState {
    DISCONNECTED = 'NoConnection',
    CONNECTING = 'Connecting',
    CONNECTED = 'Connected'
}

export enum AsrGrammarInteractionState {
    READY = 'Ready',
    RECORDING = 'Recording',
    RECOGNIZING = 'Recognizing'
}

export interface AsrSetGrammarResult {
    // Represents the state after calling `setGrammar` until a new call.
    // This is a context in which the state is used, necessary when having multiple (visible) record buttons
    result$: Observable<AsrResult>;
    intermediateResult$?: Observable<AsrIntermediateResult>;
}
