import { ConnectionQualityChangedEvent, events, IntermediateResultEvent, TransportLostConnectionEvent, TransportStateChangedEvent } from './events/events.constants';
import { EventBus } from './events/novo-speech.event-bus';
import { AsrIntermediateResult, AsrResult } from './models/asr-result';
import { AsrConnectionQuality } from './models/types';
import { Deferred, Json, promiseTimeout } from './models/utilities';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import { AsrTransportState } from './novo-speech.interfaces';

export interface TransportRequest {
    request: any;
    timestamp: Date;
    defer: Deferred<any>;
}

export class NovoSpeechTransport {
    private id = 0;
    private requests: { [key: number]: TransportRequest } = {};
    private websock?: WebSocket;
    private activeGrammarInteraction: NovoSpeechGrammarInteraction | null = null;
    private pollingTimer?: number;
    private pollingPromise?: Promise<any>;

    readonly eventBus: EventBus;

    // tslint:disable-next-line: variable-name
    private _internalConnQuality: AsrConnectionQuality = 'medium';
    get connectionQuality(): AsrConnectionQuality {
        return this._internalConnQuality;
    }
    set connectionQuality(quality: AsrConnectionQuality) {
        if (this._internalConnQuality === quality) { return; }
        this.eventBus.dispatch<ConnectionQualityChangedEvent>(events.CONNECTION_QUALITY_CHANGED, {
            quality,
            previous: this._internalConnQuality
        });
        this._internalConnQuality = quality;
    }

    get connected(): boolean {
        return this.state === AsrTransportState.CONNECTED;
    }

    // tslint:disable-next-line: variable-name
    private _internalState: AsrTransportState = AsrTransportState.DISCONNECTED;
    get state(): AsrTransportState {
        return this._internalState;
    }
    set state(state: AsrTransportState) {
        if (this._internalState === state) { return; }
        this.eventBus.dispatch<TransportStateChangedEvent>(events.TRANSPORT_STATE_CHANGED, {
            state,
            previous: this._internalState
        });
        this._internalState = state;
    }

    constructor(
        readonly url: string,
        // tslint:disable-next-line: variable-name
        _eventBus: EventBus = new EventBus()
    ) {
        this.eventBus = _eventBus.channel(`T-${url}`);
    }

    get isGrammarSet(): boolean {
        return this.activeGrammarInteraction != null;
    }

    get activeGrammarCtx(): NovoSpeechGrammarInteraction | null {
        return this.activeGrammarInteraction;
    }

    connect(): Promise<void> {
        if (this.connected) {
            console.warn(`Transport can't connect when already connected.`);
            return Promise.resolve();
        }

        const connecting = new Deferred<void>();
        this.state = AsrTransportState.CONNECTING;

        this.websock = new WebSocket(this.url);
        this.websock.binaryType = 'arraybuffer';
        this.activeGrammarInteraction = null;

        this.websock.onopen = () => {
            this.state = AsrTransportState.CONNECTED;
            void this.checkConnection(true);
            connecting.resolve();
        };

        this.websock.onmessage = event => {
            this.eventBus.dispatch(events.TRANSPORT_MESSAGE_RECEIVED);
            if (this.connected) {
                const result = JSON.parse(event.data);
                this.parseResult(result);
            }
        };

        this.websock.onclose = (event: CloseEvent) => {
            // 4000 is a custom error code for a user initiated close
            if (this.connected && event.code !== 4000) {
                console.log('Websocket closed:', event);
                event.preventDefault();

                connecting.reject();
                this.lostConnection({
                    reason: 'Websocket closed',
                    closeEvent: {
                        reason: event.reason,
                        code: event.code
                    }
                });
            }
        };

        this.websock.onerror = error => {
            if (this.connected) {
                console.error('Websocket error:', JSON.stringify(error));
                connecting.reject(error);
                this.lostConnection({
                    error,
                    reason: 'Websocket error'
                });
            }
        };

        return connecting.promise;
    }

    private getConnectionQuality(promise: Promise<AsrConnectionQuality>): Promise<AsrConnectionQuality> {
        return new Promise(resolve => {
            const before = new Date();
            const timer = setTimeout(() => resolve('low'), 1000);
            promise.then(() => {
                clearTimeout(timer);
                const duration = new Date().getTime() - before.getTime();
                if (duration < 200) {
                    resolve('high');
                } else {
                    resolve('medium');
                }
            }).catch(_ => {
                resolve('error');
            });
        });
    }

    async checkConnection(force = false): Promise<void> {
        if (!this.connected) {
            return;
        }

        if (this.pollingPromise && !force) {
            // Already checking connection
            return;
        }

        if (this.pollingTimer) {
            clearTimeout(this.pollingTimer);
        }

        const promise = this.getState();
        this.pollingPromise = promise;
        this.connectionQuality = await this.getConnectionQuality(promise);

        if (this.connected) {
            // Check if alive (not coming back < 20s)
            const isAlive = await promiseTimeout(20000, promise);
            this.pollingPromise = undefined;
            if (!isAlive) {
                this.lostConnection({ reason: 'Connection check timed out' });
            } else {
                this.pollingTimer = setTimeout(() => this.checkConnection(), 15000) as unknown as number;
            }
        }
    }

    private lostConnection(detail: { reason: string; error?: Event; closeEvent?: { reason: string; code: number } }): void {
        if (this.pollingTimer) {
            clearTimeout(this.pollingTimer);
        }
        if (this.websock && this.websock.readyState <= WebSocket.OPEN) {
            // Websocket is CONNECTING or OPEN
            this.websock?.close(detail.closeEvent?.code);
        }

        // Notify listeners to outstanding requests that the connection is lost.
        // They can then handle this case by notifying the user
        this.rejectOutstandingRequests();

        if (this.state === AsrTransportState.DISCONNECTED) { return; }
        this.state = AsrTransportState.DISCONNECTED;
        this.eventBus.dispatch<TransportLostConnectionEvent>(events.TRANSPORT_LOST_CONNECTION, { reason: detail.closeEvent?.reason || detail.reason, code: detail.closeEvent?.code || -1 });
    }

    private getID(): number {
        return ++this.id;
    }

    checkReadyState(): void {
        if (this.websock?.readyState !== WebSocket.OPEN) {
            this.lostConnection({ reason: 'Websocket connection not open' });
        }
    }

    clear(): void {
        this.lostConnection({ reason: 'Closed by client', closeEvent: { code: 4000, reason: 'Transport cleared' } });
    }

    request(method: string, params?: any): Promise<any> {
        const id = this.getID();
        const request = {
            jsonrpc: '2.0',
            method,
            params,
            id
        };
        this.requests[id] = {
            request,
            timestamp: new Date(),
            defer: new Deferred()
        };
        this.checkReadyState();
        if (this.connected && this.websock) {
            this.websock.send(JSON.stringify(request));
        }
        return this.requests[id].defer.promise;
    }

    async sendAudio(data: Blob | ArrayBuffer): Promise<void> {
        this.checkReadyState();
        if (this.connected) {
            this.websock?.send(data);
        }
    }

    waitForRecognitionResult(): Promise<Json<AsrResult>> {
        return this.request('get_result');
    }

    async setGrammar(grammar: NovoSpeechGrammarInteraction, params: any, returnIntermediateResults = false) {
        params.intermediate_result_settings = {
            when: returnIntermediateResults ? 'always' : 'only_at_eos',
            details: ['utterance']
        };
        const resp = await this.request('set_grammar', params);
        this.activeGrammarInteraction = grammar;
        return resp;
    }

    initCodec(params: { type: string, codec: string | null }) {
        return this.request('init_codec', params);
    }

    getState() {
        return this.request('get_fsm_state');
    }

    isEOS(path: string, finalSilenceDuration: number): boolean {
        return path.search('</s>') !== -1 && (finalSilenceDuration >= 0.3 || finalSilenceDuration == null);
    }

    parseResult(result: any) {
        if (!('id' in result) || (!(result.id in this.requests) && result.id !== -1)) {
            return;
        }

        let resultType: string;
        let request: TransportRequest;

        if (result.id === -1) {
            if (result.error) {
                throw result.error;
            }
            const path = result.result.current_best_path;
            if (path) {
                const finalSilenceDuration = result.result.final_sil_dur;
                if (this.isEOS(path, finalSilenceDuration)) {
                    this.eventBus.dispatch(events.EOS);
                }
                this.eventBus.dispatch<IntermediateResultEvent>(events.INTERMEDIATE_RESULT, {
                    ctx: this.activeGrammarInteraction?.grammarCtxId || undefined,
                    result: AsrIntermediateResult.initialize(result.result)
                });
            }
            return;
        }

        if (result.id in this.requests) {
            request = this.requests[result.id];
            resultType = request.request.method;
        } else {
            console.error('Could not interpret result', result);
            return;
        }

        switch (resultType) {
            case 'set_grammar':
                if (result.result) {
                    void this.checkConnection();
                    request.defer.resolve(result.result);
                } else {
                    request.defer.reject(result.error);
                }
                break;

            case 'get_result':
                if (result.error) {
                    request.defer.reject(result.error);
                } else {
                    result = result.result;
                    request.defer.resolve(result);
                }
                break;

            case 'init_codec':
                if (result.error) {
                    request.defer.reject(result.error);
                } else {
                    request.defer.resolve();
                }
                break;

            case 'get_fsm_state':
                if (result.error) {
                    request.defer.reject(result.error);
                } else {
                    request.defer.resolve();
                }
                break;

            default:
                console.warn('Unknown method:', request.request.method, result);
                break;
        }
        if (request != null) {
            delete this.requests[result.id];
        }
    }

    private rejectOutstandingRequests(): void {
        Object.values(this.requests).forEach(request => request.defer.reject('Connection to speech recognizer was lost'));
    }
}
