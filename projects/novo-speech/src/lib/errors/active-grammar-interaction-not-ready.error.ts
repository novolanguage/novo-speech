export class ActiveGrammarInteractionNotReady extends Error {
    constructor() {
        super(`Can't activate GrammarInteraction since the current active GrammarInteraction is not in READY state.`);
    }
}
