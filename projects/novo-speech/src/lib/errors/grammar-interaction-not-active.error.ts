export class GrammarInteractionNotActive extends Error {
    constructor() {
        super(`Can't record since GrammarInteraction is not activated. Make sure to activate this instance first.`);
    }
}
