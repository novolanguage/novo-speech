// tslint:disable: no-string-literal

import { EventBus } from './events/novo-speech.event-bus';
import { RecorderApi } from './models';
import { ConfusionNetworkGrammar } from './models/grammar';
import { NovoSpeechController } from './novo-speech.controller';
import { NovoSpeechGrammarInteraction } from './novo-speech.grammar-interaction';
import * as grammarFactories from './novo-speech.grammar-interaction-factory';
import { NovoSpeechConfig } from './novo-speech.interfaces';
import { SessionManager } from './novo-speech.session-manager';
import { NovoSpeechTransport } from './novo-speech.transport';
import { MediaRecorderWrapper } from './recorder/media-recorder-wrapper.recorder';
import { RecorderInterface } from './recorder/util/recorder.interface';

describe('NovoSpeechController', () => {
    let eb: EventBus;
    let ctrl: NovoSpeechController;
    const config: NovoSpeechConfig = {
        token: 'abc-123'
    };

    beforeEach(() => {
        ctrl = new NovoSpeechController(config);
        eb = ctrl.eventBus;
    });

    describe('#ensureSession', () => {
        let mediaSpy: jasmine.Spy;
        let sessionSpy: jasmine.Spy;

        beforeEach(() => {
            mediaSpy = spyOn(navigator.mediaDevices, 'getUserMedia').and.resolveTo(new MediaStream());
            sessionSpy = spyOn(SessionManager.prototype, 'establish').and.resolveTo();
        });

        it('sets a RecorderInstance using setupRecorderApi when not already initialized', async () => {
            const recorderSpy = spyOn(ctrl, 'setupRecorderApi').and.resolveTo(new MediaRecorderWrapper());
            await ctrl.ensureSession('nl');

            expect(recorderSpy).toHaveBeenCalled();
            expect(ctrl.recorder).toBeDefined();
        });

        it('sets a RecorderInstance when not already initialized', async () => {
            await ctrl.ensureSession('nl');

            expect(ctrl.recorder).toBeDefined();
        });

        it('uses sessionManagerFactory to establish a new instance of SessionManager', async () => {
            const factorySpy = spyOn(ctrl, 'sessionManagerFactory').and.returnValue(new SessionManager('', '', 'nl', { type: 'webm', codec: 'opus' }, undefined));

            await ctrl.ensureSession('nl');

            expect(factorySpy).toHaveBeenCalledWith('nl', { type: 'webm', codec: 'opus' }, undefined);
        });

        it('returns an established SessionManager instance', async () => {
            await ctrl.ensureSession('nl');

            expect(sessionSpy).toHaveBeenCalled();
        });

        it('passes through the set language to the SessionMananger instance', async () => {
            const session = await ctrl.ensureSession('nl');

            expect(session.targetLanguage).toEqual('nl');
        });

        it('re-uses an existing SessionManager instance when the same Language is provided', async () => {
            const session = await ctrl.ensureSession('nl');
            const sessionTwo = await ctrl.ensureSession('nl');

            expect(session).toEqual(sessionTwo);
        });

        it('creates a new instance when a different Language is provided', async () => {
            const session = await ctrl.ensureSession('nl');
            const sessionTwo = await ctrl.ensureSession('en');

            expect(session).not.toEqual(sessionTwo);
        });

        it('creates a new instance when a different sNodeId is provided', async () => {
            const session = await ctrl.ensureSession('nl');
            const sessionTwo = await ctrl.ensureSession('nl', 123);

            expect(session).not.toEqual(sessionTwo);
        });

    });

    describe('#invalidateSession', () => {
        let mediaSpy: jasmine.Spy;
        let sessionSpy: jasmine.Spy;

        beforeEach(() => {
            mediaSpy = spyOn(navigator.mediaDevices, 'getUserMedia').and.resolveTo(new MediaStream());
            sessionSpy = spyOn(SessionManager.prototype, 'establish').and.resolveTo();
        });

        it('looks up an existing SessionManager instance when language and sNodeId are provided', async () => {
            const session = await ctrl.ensureSession('nl');
            const endSpy = spyOn(session, 'end').and.resolveTo();

            await ctrl.invalidateSession('nl');

            expect(endSpy).toHaveBeenCalled();
        });

        it('silently fails when no sessionmanager instance is found', async () => {
            expect(Object.keys(ctrl.activeSessions).length).toEqual(0);
            await expectAsync(ctrl.invalidateSession('en')).toBeResolved;
        });

        it('accepts a SessionManager instance', async () => {
            const session = await ctrl.ensureSession('nl');
            const endSpy = spyOn(session, 'end').and.resolveTo();

            await ctrl.invalidateSession(session);

            expect(endSpy).toHaveBeenCalled();
        });

        it('removes the instance from the activeSessions object', async () => {
            await ctrl.ensureSession('nl', 10);

            expect(Object.keys(ctrl.activeSessions).length).toEqual(1);

            await ctrl.invalidateSession('nl', 10);

            expect(Object.keys(ctrl.activeSessions).length).toEqual(0);
        });

    });

    describe('#chooseRecorderApi', () => {

        it('returns the forced API when given', () => {
            expect(ctrl.chooseRecorderApi(RecorderApi.CORDOVA)).toEqual(RecorderApi.CORDOVA);
        });

        it('returns HTML5_WEBAUDIO when MediaRecorder is not available', () => {
            const w = window as { MediaRecorder?: unknown };
            const orig: any = w['MediaRecorder'];
            delete w['MediaRecorder'];

            expect(ctrl.chooseRecorderApi()).toEqual(RecorderApi.HTML5_WEBAUDIO);

            w['MediaRecorder'] = orig;
        });

        it('returns HTML5_WEBAUDIO when mediaDevices are not available', () => {
            spyOnProperty(navigator, 'mediaDevices').and.returnValue(undefined);

            expect(ctrl.chooseRecorderApi()).toEqual(RecorderApi.HTML5_WEBAUDIO);
        });

        it('uses HTML5_MEDIARECORDER when mediaDevices and MediaRecorder are available', () => {
            // Test browser (Chrome) has mediarecorder support
            expect(ctrl.chooseRecorderApi()).toEqual(RecorderApi.HTML5_MEDIARECORDER);
        });

        it('uses CORDOVA when mediaDevices is missing and Cordova exists in the window object', () => {
            window['cordova'] = {};
            window['device'] = { platform: 'iOS' };
            spyOnProperty(navigator, 'mediaDevices').and.returnValue(undefined);

            expect(ctrl.chooseRecorderApi()).toEqual(RecorderApi.CORDOVA);

            delete window['cordova'];
        });
    });

    describe('#setupRecorderApi', () => {

        it('calls ensurePermissions() on the provided RecorderApi', async () => {
            const ensureSpy = spyOn(MediaRecorderWrapper, 'ensurePermissions').and.resolveTo();
            spyOn(navigator.mediaDevices, 'getUserMedia').and.resolveTo(new MediaStream());

            await expectAsync(ctrl.setupRecorderApi(RecorderApi.HTML5_MEDIARECORDER)).toBeResolved();
            expect(ensureSpy).toHaveBeenCalled();
        });

        it('returns an instance of the choosen recorder', async () => {
            spyOn(MediaRecorderWrapper, 'ensurePermissions').and.resolveTo();
            spyOn(navigator.mediaDevices, 'getUserMedia').and.resolveTo(new MediaStream());

            const recorder = await ctrl.setupRecorderApi(RecorderApi.HTML5_MEDIARECORDER);

            expect(recorder).toBeInstanceOf(MediaRecorderWrapper);
        });

    });

    describe('#getForcedAlignmentGrammar', () => {
        let session: SessionManager;
        let recorder: RecorderInterface;

        beforeEach(() => {
            recorder = new MediaRecorderWrapper();

            session = new SessionManager('', '', 'nl', recorder.codec || undefined, undefined, eb);
            session.transport = new NovoSpeechTransport('', session.eventBus);


            ctrl.recorder = recorder;
        });

        it('should throw an error when the provided session has no transport instance', async () => {
            ctrl.recorder = recorder; // make sure we're not failing over the missing recorder
            session.transport = undefined;

            expect(session.transport).toBeUndefined(); // double check
            await expectAsync(
                ctrl.getForcedAlignmentGrammar(session, '')
            ).toBeRejectedWithError(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        });


        it('should (try) ensure a recorder when none is present', async () => {
            const recorderSpy = spyOn(ctrl, 'setupRecorderApi').and.resolveTo(new MediaRecorderWrapper());

            ctrl.recorder = undefined;
            session.transport = new NovoSpeechTransport('', eb); // make sure we're not failing on the missing transport

            expect(ctrl.recorder).toBeUndefined(); // double check

            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'forcedAlignmentGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getForcedAlignmentGrammar(session, '')
            ).toBeResolvedTo(grammarInteraction);

            expect(recorderSpy).toHaveBeenCalled();
            expect(ctrl.recorder).toBeDefined();
            expect(spy).toHaveBeenCalled();
        });

        it('creates a new GrammarInteraction instance using the forcedAlignmentGrammarFactory', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'forcedAlignmentGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getForcedAlignmentGrammar(session, '')
            ).toBeResolvedTo(grammarInteraction);

            expect(spy).toHaveBeenCalled();
        });


        it('activates the created GrammarInteraction instance', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            const activationSpy = spyOn(grammarInteraction, 'activate').and.resolveTo();
            spyOn(grammarFactories, 'forcedAlignmentGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getForcedAlignmentGrammar(session, '')
            ).toBeResolvedTo(grammarInteraction);

            expect(activationSpy).toHaveBeenCalled();
        });
    });

    describe('#getMultipleChoiceGrammar', () => {
        let session: SessionManager;
        let recorder: RecorderInterface;

        beforeEach(() => {
            recorder = new MediaRecorderWrapper();

            session = new SessionManager('', '', 'nl', recorder.codec || undefined, undefined, eb);
            session.transport = new NovoSpeechTransport('', session.eventBus);

            ctrl.recorder = recorder;
        });

        it('should throw an error when the provided session has no transport instance', async () => {
            ctrl.recorder = recorder; // make sure we're not failing over the missing recorder
            session.transport = undefined;

            expect(session.transport).toBeUndefined(); // double check
            await expectAsync(
                ctrl.getMultipleChoiceGrammar(session, [''])
            ).toBeRejectedWithError(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        });


        it('should (try) ensure a recorder when none is present', async () => {
            const recorderSpy = spyOn(ctrl, 'setupRecorderApi').and.resolveTo(new MediaRecorderWrapper());

            ctrl.recorder = undefined;
            session.transport = new NovoSpeechTransport('', eb); // make sure we're not failing on the missing transport

            expect(ctrl.recorder).toBeUndefined(); // double check

            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'multipleChoiceGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getMultipleChoiceGrammar(session, [''])
            ).toBeResolvedTo(grammarInteraction);

            expect(recorderSpy).toHaveBeenCalled();
            expect(ctrl.recorder).toBeDefined();
            expect(spy).toHaveBeenCalled();
        });

        it('creates a new GrammarInteraction instance using the multipleChoiceGrammarFactory', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'multipleChoiceGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getMultipleChoiceGrammar(session, [''])
            ).toBeResolvedTo(grammarInteraction);

            expect(spy).toHaveBeenCalled();
        });


        it('activates the created GrammarInteraction instance', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            const activationSpy = spyOn(grammarInteraction, 'activate').and.resolveTo();
            spyOn(grammarFactories, 'multipleChoiceGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getMultipleChoiceGrammar(session, [''])
            ).toBeResolvedTo(grammarInteraction);

            expect(activationSpy).toHaveBeenCalled();
        });
    });

    describe('#getOpenRecordingGrammar', () => {
        let session: SessionManager;
        let recorder: RecorderInterface;

        beforeEach(() => {
            recorder = new MediaRecorderWrapper();

            session = new SessionManager('', '', 'nl', recorder.codec || undefined, undefined, eb);
            session.transport = new NovoSpeechTransport('', session.eventBus);

            ctrl.recorder = recorder;
        });

        it('should throw an error when the provided session has no transport instance', async () => {
            ctrl.recorder = recorder; // make sure we're not failing over the missing recorder
            session.transport = undefined;

            expect(session.transport).toBeUndefined(); // double check
            await expectAsync(
                ctrl.getOpenRecordingGrammar(session)
            ).toBeRejectedWithError(`Provided SessionManager has no active TransportConnection. Make sure to retrieve your session using 'ensureSession()'`);
        });


        it('should (try) ensure a recorder when none is present', async () => {
            const recorderSpy = spyOn(ctrl, 'setupRecorderApi').and.resolveTo(new MediaRecorderWrapper());

            ctrl.recorder = undefined;
            session.transport = new NovoSpeechTransport('', eb); // make sure we're not failing on the missing transport

            expect(ctrl.recorder).toBeUndefined(); // double check

            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'openRecordingGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getOpenRecordingGrammar(session)
            ).toBeResolvedTo(grammarInteraction);

            expect(recorderSpy).toHaveBeenCalled();
            expect(ctrl.recorder).toBeDefined();
            expect(spy).toHaveBeenCalled();
        });

        it('creates a new GrammarInteraction instance using the openRecordingGrammarFactory', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            spyOn(grammarInteraction, 'activate').and.resolveTo();
            const spy = spyOn(grammarFactories, 'openRecordingGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getOpenRecordingGrammar(session)
            ).toBeResolvedTo(grammarInteraction);

            expect(spy).toHaveBeenCalled();
        });


        it('activates the created GrammarInteraction instance', async () => {
            const grammarInteraction = new NovoSpeechGrammarInteraction(new ConfusionNetworkGrammar(), false, session, recorder, eb);
            const activationSpy = spyOn(grammarInteraction, 'activate').and.resolveTo();
            spyOn(grammarFactories, 'openRecordingGrammarFactory').and.returnValue(grammarInteraction);

            await expectAsync(
                ctrl.getOpenRecordingGrammar(session)
            ).toBeResolvedTo(grammarInteraction);

            expect(activationSpy).toHaveBeenCalled();
        });
    });

    describe('#updateToken', () => {

        it('should update the set configuration token', () => {
            ctrl.updateToken('newtoken');

            expect(ctrl.novoSpeechConfig.token).toEqual('newtoken');
        });

        it('should not update the token for existing SessionManagers by default', () => {
            ctrl.activeSessions['one'] = new SessionManager('sometoken', 'http', 'nl', undefined);

            ctrl.updateToken('newtoken');

            expect((ctrl.activeSessions['one'] as any).headers['Authentication-Token']).toEqual('sometoken');
        });

        it('should update the token for existing SessionManagers when true is given as second param', () => {
            ctrl.activeSessions['one'] = new SessionManager('sometoken', 'http', 'nl', undefined);

            ctrl.updateToken('newtoken', true);

            expect((ctrl.activeSessions['one'] as any).headers['Authentication-Token']).toEqual('newtoken');
        });

    });

});
