import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { NovoSpeechController } from '../novo-speech.controller';
import { NovoSpeechConfig } from '../novo-speech.interfaces';

/**
 * NovoSpeechController Factory used to initialize a new
 * instance of the NovoSpeechController with the provided
 * configuration object.
 */
export function novoSpeechControllerFactory(config: NovoSpeechConfig): NovoSpeechController {
    return new NovoSpeechController(config);
};

/**
 * Configuration token
 */
export const CONFIG_TOKEN = new InjectionToken<NovoSpeechConfig>('NovoSpeechModule: CONFIG_TOKEN');


@NgModule()
export class NovoSpeechModule {

    /**
     * Use this in your main 'app module' to initialize a new singleton
     * of the NovoSpeechController service.
     */
    static forRoot(config: NovoSpeechConfig): ModuleWithProviders<NovoSpeechModule> {
        return {
            ngModule: NovoSpeechModule,
            providers: [
                {
                    provide: CONFIG_TOKEN,
                    useValue: config
                },
                {
                    provide: NovoSpeechController,
                    useFactory: novoSpeechControllerFactory,
                    deps: [CONFIG_TOKEN]
                }
            ]
        };
    }
}