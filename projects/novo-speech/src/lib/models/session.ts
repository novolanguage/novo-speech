import { Expose, plainToClass } from 'class-transformer';
import { LanguageCode } from './phone-set';
import { Json } from './utilities';

export interface Proxy {
    readonly hostname: string;
    readonly ip: string;
}

export interface Datacentre {
    readonly proxy: Proxy;
}

export interface Snode {
    readonly datacentre: Datacentre;
    readonly id: number;
}

export class AsrSession {
    readonly active: boolean;
    @Expose({ name: 'created_at' }) readonly createdAt: Date;
    @Expose({ name: 'ended_at' }) readonly endedAt: Date;
    @Expose({ name: 'initialized_at' }) readonly initializedAt: Date;
    @Expose({ name: 'updated_at' }) readonly updatedAt: Date;
    readonly id: number;
    readonly uuid: string;
    readonly port: number;
    readonly language: LanguageCode;
    readonly snode: Snode;

    static initialize(json: Json<AsrSession>, language: LanguageCode): AsrSession {
        json.language = language;
        return plainToClass<AsrSession, Json<AsrSession>>(AsrSession, json);
    }

    get proxyURL(): string {
        let proxyBaseUrl = this.snode.datacentre.proxy.ip;
        if (this.snode.datacentre.proxy.hostname) {
            proxyBaseUrl = this.snode.datacentre.proxy.hostname;
        }
        return `wss://${proxyBaseUrl}/${this.uuid}`;
    }
}
