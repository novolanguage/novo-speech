export enum RecorderApi {
    HTML5_WEBAUDIO,
    HTML5_MEDIARECORDER,
    CORDOVA,
    NONE
}
export const MULTIPLE_CHOICE = 'multiple_choice';
export const ORDER = 'word_order';
export const CONFUSION_NETWORK = 'confusion_network';
export const SESSION_TIMEOUT = 1000 * 60 * 60; // 60 minutes
export const SESSION_SETUP_TIME = 3500;

/**
 * Number of milliseconds before a recording is consider relevant
 */
export const MIN_RECORDING_TIME = 500;

/**
 * Max time for a recording before its stopped automatically
 */
export const MAX_RECORDING_TIME = 60000;
