export type PhoneSet = 'cmu69' | 'novo70' | 'novo-nl37' | 'mdbg115' | 'novo-cmn111' | 'qcri36';

export type LanguageCode =
    | 'ar'
    | 'en'
    | 'zh'
    | 'nl';

export const phonesetMap: { [languageCode in LanguageCode]?: PhoneSet } = {
    en: 'cmu69',
    zh: 'novo-cmn111',
    nl: 'novo-nl37',
    ar: 'qcri36'
};
