export type Json<T> = { -readonly [P in keyof T]?: T[P] extends Function ? never : T[P] };

export const initConstructor = <T extends object>(obj: T, data: T) => {
    Object.keys(data).forEach(key => {
        // Only properties of data which are defined (so we do not override initial values)
        if (data.hasOwnProperty(key) && (data as any)[key] != null) {
            (obj as any)[key] = (data as any)[key];
        }
    });
};

export const serializeType = <T>(object: T) => () => object;

export class Deferred<T> {
    promise: Promise<T>;
    reject!: (reason?: any) => void;
    resolve!: (value?: T | PromiseLike<T>) => void;

    constructor() {
        this.promise = new Promise<T>((resolve, reject) => {
            this.reject = reject;
            this.resolve = resolve;
        });
    }
}

/**
 * Wraps a promise in a timeout, allowing the promise to reject if not resolve with a specific period of time
 * @param ms - milliseconds to wait before rejecting promise if not resolved
 * @param promise to monitor
 */
export function promiseTimeout(ms: number, promise: Promise<any>): Promise<boolean> {
    return new Promise((resolve, reject) => {
        // create a timeout to reject promise if not resolved
        const timer = setTimeout(() => {
            resolve(false);
        }, ms);

        promise.then(() => {
            clearTimeout(timer);
            resolve(true);
        }).catch(() => {
            clearTimeout(timer);
            resolve(false);
        });
    });
}
