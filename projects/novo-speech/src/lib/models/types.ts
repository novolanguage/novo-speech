export type NovoSpraakServerState =
    | 'closed'
    | 'ready_for_grammar_or_codec'
    | 'ready_for_audio_or_grammar'
    | 'ready_for_audio_or_eos'
    | 'busy_finalizing';

export type AsrConnectionQuality = 'high' | 'medium' | 'low' | 'error';
