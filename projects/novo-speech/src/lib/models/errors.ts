export enum NovoSpraakErrorCode {
    NotOwner,
    NotAuthorized,
    SessionNotFound,
    TooManyConcurrentSessions,
    UnknownError
}

export class NovoSpraakError extends Error {
    readonly message: string;
    readonly code: NovoSpraakErrorCode;

    constructor(message: string, code: NovoSpraakErrorCode) {
        super(message);
        this.message = message;
        this.code = code;
    }
}
