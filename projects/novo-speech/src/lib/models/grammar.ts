import { PhoneSet } from '../models';


export class AsrGrammar { }

export class ConfusionNetworkGrammar extends AsrGrammar {
    version = '1.0';
    type = 'confusion_network';
    phoneset: PhoneSet;
    parallel_silence = true;
    data: ConfusionGrammarData;
    return_objects: ('dict' | 'grammar')[] = [];
}

export class OpenRecordingGrammar extends AsrGrammar {
    type = 'recording';
    data = {};
}

export class ConfusionGrammarData {
    kind: 'sequence' | 'alternatives' | 'order';
    elements: ConfusionGrammarGroup[] | AsrGrammarWord[];
    meta: any;
}

export class ConfusionGrammarGroup {
    kind: 'sequence' | 'alternatives' | 'order';
    elements: ConfusionGrammarGroup[] | AsrGrammarWord[];
    meta: any;
}

export class AsrGrammarWord {
    id: number;
    kind = 'word';
    label: string;
    pronunciation: AsrGrammarPronunciation[];
    syllables: AsrGrammarSyllable[];
    graphemes: string[];
    meta: any;

    constructor(label: string, pronunciations?: AsrGrammarPronunciation[]) {
        this.label = label;
        this.pronunciation = pronunciations;
    }
}

export class AsrGrammarPronunciation {
    id: number;
    phones: string[];
    syllables: AsrGrammarSyllable[];
    meta: any;

    constructor(phones: string[], correct: boolean) {
        this.phones = phones;
        this.meta = { correct };
    }
}

export interface AsrGrammarSyllable {
    begin: number;
    end: number;
    stress: number;
    tone: number;
}
