import { Exclude, Expose, plainToClass, Type } from 'class-transformer';
import { LanguageCode, phonesetMap } from './phone-set';
import { PhoneInventory } from './phones';
import { AsrSession } from './session';
import { initConstructor, serializeType } from './utilities';


export interface AsrConfidence {
    llr: number;
    prob: number;
}

export class AsrAudioFile {

    @Expose()
    get url() {
        if (this._url) { return this._url; }
        if (this._id) {
            const [sessionUUID, taskUUID] = this._id.substr(6).split('/');
            return `https://gm.novo-learning.com/v0/sessions/${sessionUUID}/audio/${taskUUID}`;
        }
    }
    @Exclude()
    private _id: string;

    @Exclude()
    private _url: string;

    static fromURL(url: string): AsrAudioFile {
        const file: AsrAudioFile = new AsrAudioFile();
        file._url = url;
        return file;
    }

    static fromId(id: string): AsrAudioFile {
        const file: AsrAudioFile = new AsrAudioFile();
        file._id = id;
        return file;
    }

    static initialize(value: string | { url: string }): AsrAudioFile | undefined {
        if (typeof (value) === 'string') {
            return AsrAudioFile.fromId(value);
        } else if (value.url != null) {
            return AsrAudioFile.fromURL(value.url);
        }
    }

    setUrl(url: string) {
        this._url = url;
    }

    getFastAudioUrl(currentSession: AsrSession, local = false) {
        if (this._id) {
            if (!local) {
                return `https://${currentSession.snode.datacentre.proxy.hostname}/${this._id}`;
            } else {
                return `https://${currentSession.snode.datacentre.proxy.ip}/${this._id}`;
            }
        }
    }
}

export interface AsrAudio {
    mp3?: AsrAudioFile;

    ogg?: AsrAudioFile;

    wav?: AsrAudioFile;

    webm?: AsrAudioFile;

    spx?: AsrAudioFile;
}

export interface AsrRecognized {
    raw: string;
    ipa?: string;
}

export interface AsrRecognizedPhone {
    label: string;
    confidence: AsrConfidence;
}

export interface AsrPhone {
    begin: number;
    end: number;
    label: string;
    confidence: AsrConfidence;
    recognized: AsrRecognizedPhone;
    correct: boolean;
}

interface AsrWordModel {
    begin: number;
    end: number;
    label: AsrRecognized;
    confidence: AsrConfidence;
    phones: AsrPhone[];
    isSpeech: boolean;
}

export class AsrWord implements AsrWordModel {
    begin: number;
    end: number;
    label: AsrRecognized;
    confidence: AsrConfidence;
    phones: AsrPhone[];
    isSpeech: boolean;

    static initialize(json: AsrWordModel) {
        return new AsrWord({ ...json });
    }

    constructor(data: AsrWordModel) {
        initConstructor<AsrWordModel>(this, data);
    }
}

export class AsrResult {
    confidence: AsrConfidence;
    audio: AsrAudio;
    recognized: AsrRecognized;
    words: AsrWord[];
    @Expose({ name: 'task_uuid' }) readonly taskUUID: string;
    @Type(serializeType(Date)) receivedAt: Date;

    static initialize(json: any): AsrResult {
        const result = new AsrResult(json.task_uuid);

        result.confidence = json.confidence;
        if (json.audio) {
            result.audio = {
                mp3: json.audio.mp3 && AsrAudioFile.initialize(json.audio.mp3),
                ogg: json.audio.ogg && AsrAudioFile.initialize(json.audio.ogg),
                wav: json.audio.wav && AsrAudioFile.initialize(json.audio.wav),
                webm: json.audio.webm && AsrAudioFile.initialize(json.audio.webm),
                spx: json.audio.spx && AsrAudioFile.initialize(json.audio.spx)
            };
        }
        result.recognized = json.recognized;
        result.words = json.words ? json.words.map(wordJson => AsrWord.initialize(wordJson)) : undefined;
        if (json.receivedAt == null) {
            result.receivedAt = new Date();
        } else {
            result.receivedAt = new Date(json.receivedAt);
        }
        result.process();
        return result;
    }

    constructor(taskUUID: string) {
        this.taskUUID = taskUUID;
    }

    isSpeech(s: string): boolean {
        return !(s === '<sil>' || s === '<s>' || s === '</s>' || s === '!sil');
    }

    process() {
        if (this.words) {
            this.words.forEach((w: AsrWord) => {
                w.isSpeech = this.isSpeech(w.label.raw);
            });
        }
    }

    toString(): string {
        let s = '';
        this.words.forEach((w: AsrWord) => {
            s += `${(w.begin / 1000).toFixed(3)}\t${(w.end / 1000).toFixed(3)}\t${w.confidence.llr.toFixed(3)}\t${w.confidence.prob.toFixed(2)}\t${w.label.raw}\n`;
            w.phones.forEach(p => {
                s += `${(p.begin / 1000).toFixed(3)}\t${(p.end / 1000).toFixed(3)}\t${p.confidence.llr.toFixed(3)}\t${p.confidence.prob.toFixed(2)}\t${p.label}\n`;
            });
            s += `\n`;
        });
        return s;
    }

    addIpa(languageCode: LanguageCode) {
        const phonesetCode = phonesetMap[languageCode];
        if (phonesetCode) {
            const phoneset = new PhoneInventory(phonesetCode);
            phoneset.map.sil = { label: 'sil', baseLabel: 'sil', ipa: '_', exampleWord: '<sil>', isVowel: false };
            this.words.forEach(word => {
                if (!word.label.ipa && word.phones && word.phones.length > 0) {
                    word.label.ipa = word.phones.map(p => phoneset.map[p.label].ipa).join('');
                }
            });
        }
    }
}

export class AsrIntermediateResult {
    @Expose({ name: 'current_best_path' }) readonly currentBestPath: string;
    @Type(serializeType(AsrResult)) result: AsrResult;
    @Expose({ name: 'final_sil_dur' }) readonly finalSilenceDuration: string;
    @Expose({ name: 'fsm_state' }) readonly fsmState: string;
    samples: number;
    success: boolean;
    receivedAt: Date;

    public static initialize(json): AsrIntermediateResult {
        const res: AsrIntermediateResult = plainToClass(AsrIntermediateResult, json);
        res.receivedAt = new Date();
        res.result.process();
        return res;
    }
}
