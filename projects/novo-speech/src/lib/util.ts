import { AsrIntermediateResult, AsrResult } from './models/asr-result';

/**
 * Checks if the given model is an intermediate result
 */
export const isIntermediateAsrResult = (result: AsrResult | AsrIntermediateResult): result is AsrIntermediateResult => {
    return result instanceof AsrIntermediateResult || 'currentBestPath' in result;
};