// tslint:disable: no-non-null-assertion

import { events } from './events/events.constants';
import { EventBus } from './events/novo-speech.event-bus';
import { openRecordingGrammarFactory } from './novo-speech.grammar-interaction-factory';
import { AsrTransportState } from './novo-speech.interfaces';
import { SessionManager } from './novo-speech.session-manager';
import { NovoSpeechTransport } from './novo-speech.transport';
import { MediaRecorderWrapper } from './recorder/media-recorder-wrapper.recorder';

describe('NovoSpeechTransport', () => {
    let tl: NovoSpeechTransport;
    let eb: EventBus;

    beforeEach(() => {
        eb = new EventBus();
        tl = new NovoSpeechTransport('ws://test.com', eb);
    });

    describe('connectionQuality', () => {

        it('dispatches a CONNECTION_QUALITY_CHANGED event when changing the quality', () => {
            const dispatchSpy = spyOn(eb, 'dispatch');

            tl.connectionQuality = 'low';

            expect(dispatchSpy).toHaveBeenCalledWith(events.CONNECTION_QUALITY_CHANGED, {
                quality: 'low',
                previous: 'medium'
            });
        });

        it('does not dispatch an event when quality remains the same', () => {
            const dispatchSpy = spyOn(eb, 'dispatch');

            tl.connectionQuality = tl.connectionQuality;

            expect(dispatchSpy).not.toHaveBeenCalled();
        });

    });

    describe('state', () => {

        it('dispatches a TRANSPORT_STATE_CHANGED event when changing the state', () => {
            const dispatchSpy = spyOn(eb, 'dispatch');

            tl.state = AsrTransportState.CONNECTING;

            expect(dispatchSpy).toHaveBeenCalledWith(events.TRANSPORT_STATE_CHANGED, {
                state: AsrTransportState.CONNECTING,
                previous: AsrTransportState.DISCONNECTED,
            });
        });

        it('does not dispatch an event when quality remains the same', () => {
            const dispatchSpy = spyOn(eb, 'dispatch');

            tl.state = tl.state;

            expect(dispatchSpy).not.toHaveBeenCalled();
        });

    });

    describe('connected', () => {

        it('returns true when state equals AsrTransportState.CONNECTED', () => {
            tl.state = AsrTransportState.CONNECTED;

            expect(tl.connected).toBeTrue();
        });

    });

    describe('isGrammarSet', () => {

        it('returns true when activeGrammarInteraction is not null', () => {
            const recorder = new MediaRecorderWrapper();
            (tl as any).activeGrammarInteraction = openRecordingGrammarFactory(
                new SessionManager('', '', 'nl', recorder.codec || undefined, undefined, eb),
                recorder,
                eb
            );

            expect(tl.isGrammarSet).toBeTrue();
        });

    });

    describe('activeGrammarCtx', () => {

        it('returns the currently defined grammarInteraction', () => {
            const recorder = new MediaRecorderWrapper();
            const gi = openRecordingGrammarFactory(
                new SessionManager('', '', 'nl', recorder.codec || undefined, undefined, eb),
                recorder,
                eb
            );
            (tl as any).activeGrammarInteraction = gi;

            expect(tl.activeGrammarCtx).toEqual(gi);
        });

    });

    describe('#connect', () => {

        it('logs a warning and resolves when already connected', async () => {
            const warnSpy = spyOn(console, 'warn');
            spyOnProperty(tl, 'connected').and.returnValue(true);

            await expectAsync(tl.connect()).toBeResolvedTo();
            expect(warnSpy).toHaveBeenCalledWith(`Transport can't connect when already connected.`);
        });

        it('sets the state to connecting', () => {
            tl.connect();

            expect(tl.state).toEqual(AsrTransportState.CONNECTING);
        });

        it('creates a new WebSocket instance', () => {
            tl.connect();
            const websock: WebSocket = (tl as any).websock;

            expect(websock).toBeInstanceOf(WebSocket);
            expect(websock.binaryType).toEqual('arraybuffer');
        });

        it('sets activeGrammer to null', () => {
            tl.connect();

            expect(tl.activeGrammarCtx).toBeNull();
        });

        describe('onopen() clb', () => {
            let checkConnectionSpy: jasmine.Spy;
            let websock: WebSocket;
            let returnPromise: Promise<void>;

            beforeEach(() => {
                checkConnectionSpy = spyOn(tl, 'checkConnection');

                returnPromise = tl.connect();
                websock = (tl as any).websock;
            });

            it('changes the state to CONNECTED', () => {
                websock.onopen!(new Event('ready'));

                expect(tl.state).toEqual(AsrTransportState.CONNECTED);
            });

            it('calls checkConnection(true)', () => {
                websock.onopen!(new Event('ready'));

                expect(checkConnectionSpy).toHaveBeenCalledWith(true);
            });

            it('resolves the returned promise', async () => {
                websock.onopen!(new Event('ready'));

                await expectAsync(returnPromise).toBeResolvedTo();
            });
        });

    });

    describe('onclose() clb', () => {
        let lostConnSpy: jasmine.Spy;
        let connectedSpy: jasmine.Spy;
        let websock: WebSocket;
        let returnPromise: Promise<void>;

        beforeEach(() => {
            lostConnSpy = spyOn<any>(tl, 'lostConnection');
            connectedSpy = spyOnProperty(tl, 'connected').and.returnValue(false);

            returnPromise = tl.connect();
            websock = (tl as any).websock;

            connectedSpy.and.returnValue(true); // make sure we're "connected"
        });

        it('calls lostConnection()', () => {
            websock.onclose!(new CloseEvent('close', { code: 24, reason: 'calm down' }));

            expect(lostConnSpy).toHaveBeenCalledWith({
                reason: 'Websocket closed',
                closeEvent: {
                    code: 24,
                    reason: 'calm down'
                }
            });
        });

        it('does not call lostConnection() when not connected', () => {
            connectedSpy.and.returnValue(false);

            websock.onclose!(new CloseEvent('close', { code: 24, reason: 'calm down' }));

            expect(lostConnSpy).not.toHaveBeenCalled();
        });

        it('rejects the returned promise', async () => {
            websock.onclose!(new CloseEvent('close'));

            await expectAsync(returnPromise).toBeRejected();
        });

    });

    describe('onmessage() clb', () => {
        let parseResultSpy: jasmine.Spy;
        let connectedSpy: jasmine.Spy;
        let websock: WebSocket;
        let returnPromise: Promise<void>;

        beforeEach(() => {
            parseResultSpy = spyOn<any>(tl, 'parseResult');
            connectedSpy = spyOnProperty(tl, 'connected').and.returnValue(false);

            returnPromise = tl.connect();
            websock = (tl as any).websock;

            connectedSpy.and.returnValue(true); // make sure we're "connected"
        });

        it('dispatches a TRANSPORT_MESSAGE_RECIEVED event', () => {
            const dispatchSpy = spyOn(eb, 'dispatch');

            websock.onmessage!(new MessageEvent('msg'));

            expect(dispatchSpy).toHaveBeenCalledWith(events.TRANSPORT_MESSAGE_RECEIVED);
        });

        it('parses the result when connected', () => {
            websock.onmessage!(new MessageEvent('msg', {
                data: JSON.stringify({ a: true })
            }));

            expect(parseResultSpy).toHaveBeenCalledWith({ a: true });
        });

        it('does not call parseResult when not connected', () => {
            connectedSpy.and.returnValue(false);

            websock.onmessage!(new MessageEvent('msg', {
                data: JSON.stringify({ a: true })
            }));

            expect(parseResultSpy).not.toHaveBeenCalled();
        });

    });

    describe('onerror() clb', () => {
        let lostConnSpy: jasmine.Spy;
        let connectedSpy: jasmine.Spy;
        let websock: WebSocket;
        let returnPromise: Promise<void>;

        beforeEach(() => {
            lostConnSpy = spyOn<any>(tl, 'lostConnection');
            connectedSpy = spyOnProperty(tl, 'connected').and.returnValue(false);

            returnPromise = tl.connect();
            websock = (tl as any).websock;

            connectedSpy.and.returnValue(true); // make sure we're "connected"
        });

        it('rejects the connect() promise', async () => {
            websock.onerror!(new Event('failed'));

            await expectAsync(returnPromise).toBeRejectedWith(new Event('failed'));
        });

        it('calls lostConnection()', () => {
            websock.onerror!(new Event('failed'));

            expect(lostConnSpy).toHaveBeenCalledWith({
                error: new Event('failed'),
                reason: 'Websocket error'
            });
        });

        it('does not call lostConnection() when not connected', () => {
            connectedSpy.and.returnValue(false);

            websock.onerror!(new Event('failed'));

            expect(lostConnSpy).not.toHaveBeenCalled();
        });

    });

    describe('#getConnectionQuality', () => {
        let startTime: number;

        beforeEach(() => {
            jasmine.clock().uninstall();

            const startDate = new Date();
            startTime = startDate.getTime();

            jasmine.clock().install();
            jasmine.clock().mockDate(startDate);
        });

        afterEach(() => {
            jasmine.clock().mockDate(undefined);
        });

        it('should resolve with "low" when the provided promise take over one second to fulfil', async () => {
            const prom = new Promise((res) => setTimeout(res, 1001));

            const quality = (tl as any).getConnectionQuality(prom);
            jasmine.clock().mockDate(new Date(startTime + 1100));
            jasmine.clock().tick(1010);

            await expectAsync(quality).toBeResolvedTo('low');
        });

        it('should resolve with "medium" when the provided promise take between 200 and 1000ms to fulfil', async () => {
            const prom = new Promise((res) => setTimeout(res, 500));

            const quality = (tl as any).getConnectionQuality(prom);
            jasmine.clock().mockDate(new Date(startTime + 500));
            jasmine.clock().tick(550);

            await expectAsync(quality).toBeResolvedTo('medium');
        });

        it('should resolve with "high" when the provided promise take less than 200ms to fulfil', async () => {
            const prom = new Promise((res) => setTimeout(res, 100));

            const quality = (tl as any).getConnectionQuality(prom);
            jasmine.clock().mockDate(new Date(startTime + 10));
            jasmine.clock().tick(101);

            await expectAsync(quality).toBeResolvedTo('high');
        });

    });

    describe('#checkConnection', () => {

    });

    describe('#lostConnection', () => {

    });

    describe('#getID', () => {

        it('returns an incremental number on each call', () => {
            const first = (tl as any).getID();
            const second = (tl as any).getID();

            expect(second).not.toEqual(first);
            expect(second - first).toEqual(1);
        });
    });

    describe('#checkReadyState', () => {

        it('executes lostConnection when the current WebSock readyState does not equal WebSocket.OPEN', () => {
            const lostConnSpy = spyOn<any>(tl, 'lostConnection');
            (tl as any).websock = {
                readyState: WebSocket.CONNECTING
            };

            tl.checkReadyState();

            expect(lostConnSpy).toHaveBeenCalledWith({ reason: 'Websocket connection not open' });
        });

        it('does not execute lostConnection when WebSocket is OPEN', () => {
            const lostConnSpy = spyOn<any>(tl, 'lostConnection');
            (tl as any).websock = {
                readyState: WebSocket.OPEN
            };

            tl.checkReadyState();

            expect(lostConnSpy).not.toHaveBeenCalled();
        });

    });

    describe('#clear', () => {

        it('sets the state DISCONNECTED', () => {
            tl.clear();

            expect(tl.state).toEqual(AsrTransportState.DISCONNECTED);
        });

        it('clears the pollingTimer timeout', () => {
            const clearSpy = spyOn(window, 'clearTimeout');

            (tl as any).pollingTimer = 10;
            tl.clear();

            expect(clearSpy).toHaveBeenCalledWith(10 as any);
        });

        it('calls close() on the current WebSocket', () => {
            const ws = new WebSocket('ws://com.com');
            const closeSpy = spyOn(ws, 'close');

            (tl as any).websock = ws;

            tl.clear();

            expect(closeSpy).toHaveBeenCalled();
        });
    });

});
