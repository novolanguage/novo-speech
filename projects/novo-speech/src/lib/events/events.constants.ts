import { AsrIntermediateResult } from '../models/asr-result';
import { AsrConnectionQuality } from '../models/types';
import { NovoSpeechGrammarInteraction } from '../novo-speech.grammar-interaction';
import { AsrGrammarInteractionState, AsrSessionState, AsrTransportState } from '../novo-speech.interfaces';

export const events = {
    EOS: 'NOVOSPRAAK:EOS',
    INTERMEDIATE_RESULT: 'NOVOSPRAAK:INTERMEDIATE_RESULT',
    MIC_CLIPPING: 'NOVOSPRAAK:MIC_CLIPPING',

    PROGRESS_GET_SESSION_COMPLETE: 'NOVOSPRAAK:PROGRESS_GET_SESSION_COMPLETE',
    PROGRESS_WEBSOCKET_COMPLETE: 'NOVOSPRAAK:PROGRESS_WEBSOCKET_COMPLETE',
    PROGRESS_GETUSERMEDIA_COMPLETE: 'NOVOSPRAAK:PROGRESS_GETUSERMEDIA_COMPLETE',

    TRANSPORT_LOST_CONNECTION: 'NOVOSPRAAK:TRANSPOR_LOST_CONNECTION',
    TRANSPORT_MESSAGE_RECEIVED: 'NOVOSPRAAK:TRANSPORT_MESSAGE_RECEIVED',

    SESSION_STATE_CHANGED: 'NOVOSPRAAK:SESSION_STATE_CHANGED',
    CONNECTION_QUALITY_CHANGED: 'NOVOSPRAAK:CONNECTION_QUALITY_CHANGED',
    TRANSPORT_STATE_CHANGED: 'NOVOSPRAAK:TRANSPORT_STATE_CHANGED',
    GRAMMAR_INTERACTION_STATE_CHANGED: 'NOVOSPRAAK:GRAMMAR_INTERACTION_STATE_CHANGED',

    ACTIVATING_GRAMMAR_SET: 'NOVOSPRAAK:ACTIVATING_GRAMMAR_SET',
    ACTIVATING_GRAMMAR_SET_FAILED: 'NOVOSPRAAK:ACTIVATING_GRAMMAR_SET_FAILED',
    ACTIVATED_GRAMMAR_SET: 'NOVOSPRAAK:ACTIVATED_GRAMMAR_SET'
};

export interface IntermediateResultEvent {
    result: AsrIntermediateResult;
    ctx?: string;
}

export interface SessionStateChangedEvent {
    ctx: string;
    previous?: AsrSessionState;
    state: AsrSessionState;
}

export interface GrammarInteractionStateChangedEvent {
    ctx: string;
    previous?: AsrGrammarInteractionState;
    state: AsrGrammarInteractionState;
}

export interface ConnectionQualityChangedEvent {
    previous?: AsrConnectionQuality;
    quality: AsrConnectionQuality;
}

export interface TransportStateChangedEvent {
    previous?: AsrTransportState;
    state: AsrTransportState;
}

export interface ActivatingGrammarSetEvent {
    grammar: NovoSpeechGrammarInteraction;
}

export interface ActivatingGrammarSetFailedEvent {
    error: string;
}

export interface ActivatedGrammarSetEvent {
    grammar: NovoSpeechGrammarInteraction;
}

export interface TransportLostConnectionEvent {
    reason: string;
    code: number;
}
