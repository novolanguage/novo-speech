# Api definition

The novo API is a combination of HTTP requests and a websocket connection. 

## Creating a `Client User`

Before you can use the library in your client application you'll have to create a `Client User`. In order to create this token you need your `Publisher ID` and `Publisher Secret Token`, which you received from [NovoLearning](https://novo-learning.com/contact).

Perform the following CURL request to create a new `Client User`:

```bash
curl -X POST 'https://gm.novo-learning.com/v0/publishers/${publisherId}/users'
  -H 'Content-Type: application/json;charset=utf-8'
  -H 'Authentication-Token': ${publisherSecretToken}'
  --data-binary '{"username": ${username}, "password": ${password}}'
```

> **Note: never publish your `Publisher Secret Token`, since it allows others to sign up and delete user accounts**

## Generating a `Client User Token`

After signing up a user you can generate a `Client User Token`. This token is used by the library to make calls on behalf of your user.

Create a new user by performing the following request:

```bash
curl -X POST 'https://gm.novo-learning.com/v0/publishers/${publisherId}/login'
  -H 'Content-Type: application/json;charset=utf-8'
  --data-binary '{"username": ${username}, "password": ${password}}'
```

The returned token allows your user to start ASR sessions. Provide this token to your frontend application.

By default a user is allowed to run a maximum of 5 ASR session at the same time so make sure to generate a unique token _per user_.

## Establish a session

To communicate with the back-end a series of steps needs to be performed. 
1. Create a session
2. Set the audio codec
3. Set the grammar (the type of recognition that needs to be done) and extra parameters
4. Check the state regularly. 
5. Send audio data (binary)
6. Use intermediate results (optional)
7. Get result (result of the recognition)
8. Kill the session (optional)


### Create the session
POST to `https://gm.novo-learning.com/v0/sessions/sessions` (see session.api.yaml)


### Connect to the websocket
Use the session response from the session POST to setup a connection to `{snode.datacentre.proxy.hostname}/{uuid}`

The websocket has a protocol where all messages are answered with result messages. The result messages have the same id as the request messages except for intermediate result messages (for recognition), which have -1.


### Initialize the speech audio codec
Once you've established your session it's time to initialize a codec for audio recording. 
To do this send a set grammar request over the websocket `asr.api.yaml:#/components/schemas/initCodecRequest`. 


### Initialize a Grammar Interaction
Once you've established your session it's time to initialize a Grammer Interaction. 
To do this send a set grammar request over the websocket `asr.api.yaml:#/components/schemas/setGrammarRequest`. 

There are currently three GrammarInteraction types supported which can be initialized

### Checking state
Checking state is done for a number of reasons:
- To keep the websocket open. 
- To gauge the connection quality; a ping of less than 200ms is considered indicative of a good quality, between 200ms and 1000ms medium, and above 1000ms low

#### Multiple Choice Grammar
Example 

```json
{
    "jsonrpc": "2.0",
    "method": "set_grammar",
    "params": {
        "version": "1.0",
        "type": "confusion_network",
        "parallel_silence": true,
        "return_objects": [],
        "data": {
            "kind": "alternatives",
            "elements": [
                {
                    "kind": "sequence",
                    "elements": [
                        {
                            "kind": "word",
                            "label": "Single"
                        },
                        {
                            "kind": "word",
                            "label": "choice"
                        }
                    ]
                },
                {
                    "kind": "sequence",
                    "elements": [
                        {
                            "kind": "word",
                            "label": "Multiple"
                        },
                        {
                            "kind": "word",
                            "label": "choice"
                        }
                    ]
                },
                {
                    "kind": "sequence",
                    "elements": [
                        {
                            "kind": "word",
                            "label": "Choose"
                        },
                        {
                            "kind": "word",
                            "label": "one"
                        }
                    ]
                },
                {
                    "kind": "sequence",
                    "elements": [
                        {
                            "kind": "word",
                            "label": "Choose"
                        },
                        {
                            "kind": "word",
                            "label": "several"
                        }
                    ]
                }
            ]
        },
        "intermediate_result_settings": {
            "when": "only_at_eos",
            "details": [
                "utterance"
            ]
        }
    },
    "id": 3
}
```

Call this method to initialize a new MultipleChoice GrammarInteraction. This interaction type can be used to recognize whether the user said one of the given options.


#### Forced Alignment Grammar
Example

```json
{
    "jsonrpc": "2.0",
    "method": "set_grammar",
    "params": {
        "version": "1.0",
        "type": "confusion_network",
        "parallel_silence": true,
        "return_objects": [],
        "data": {
            "kind": "sequence",
            "elements": [
                {
                    "kind": "word",
                    "label": "Hallo",
                    "pronunciation": [ 
                        {
                            "phones": [ //example of correct pronunciation
                                "hh",
                                "a",
                                "l",
                                "l",
                                "ow",
                                "ow"
                            ],
                            "meta": {
                                "correct": true
                            }
                        },
                        {
                            "phones": [ //example of incorrect pronunciation
                                "hh",
                                "a",
                                "l",
                                "l",
                                "ow"
                            ],
                            "meta": {
                                "correct": false
                            }
                        }
                    ]
                }
            ]
        },
        "intermediate_result_settings": {
            "when": "only_at_eos",
            "details": [
                "utterance"
            ]
        }
    },
    "id": 3
}
```

Initializes a new Forced Alignment GrammarInteraction. This interaction assumes the learner will say the given text, and can be used to obtain detailed pronunciation information in the form of word- and phone level confidence scores.

## Send audio
Simply send binary audio data in the designated codec over the websocket

## Intermediary recognition result

`asr.api.yaml:#/components/schemas/intermediateResult` describes the format for an intermediate ASR result. This result is sent multiple times during recognition if this is requested in the set_grammar

## Recognition result

`websocket.api.yaml:#/components/schemas/getResult` results in a definitive ASR result. `asr.api.yaml:#/components/schemas/asrResult`.

## Killing the session
- DELETE to `https://gm.novo-learning.com/v0/sessions/sessions/{id}` (see session.api.yaml)
- Sessions will be automatically cleaned up after 30 minutes of idle time so it isn't strictly necessary to kill a session. However, as a user can have 5 parallel sessions, it is advised to actively kill sessions.


