/**
 * Copy this file as 'environment.ts' and provide it with
 * your own user client token
 */

export const environment = {
  production: false,
  novoSpeechToken: '<!-- an user client token, see README.md -->'
};
