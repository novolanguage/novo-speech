import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NovoSpeechModule } from '../../../novo-speech/src/public-api';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { InteractionComponent } from './interaction/interaction.component';

@NgModule({
  declarations: [
    AppComponent,
    InteractionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NovoSpeechModule.forRoot({
      token: environment.novoSpeechToken
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
