import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BehaviorSubject, combineLatest, EMPTY, from, Observable, ReplaySubject } from 'rxjs';
import { catchError, distinctUntilChanged, filter, map, shareReplay, startWith, switchMap } from 'rxjs/operators';
import { AsrGrammarInteractionState, AsrIntermediateResult, AsrResult, AsrSessionState, isIntermediateAsrResult, LanguageCode, NovoSpeechController, NovoSpeechGrammarInteraction, SessionManager } from '../../../../novo-speech/src/public-api';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InteractionComponent implements OnInit, OnChanges {

  debugResults: boolean;

  readonly exercise$: ReplaySubject<{ options: string[] }> = new ReplaySubject(1);

  error$: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);

  readonly language$: ReplaySubject<LanguageCode> = new ReplaySubject<LanguageCode>(1);

  /**
   * Setup a session for the current Language and share it with any
   * subsequent subscribers.
   */
  readonly session$: Observable<SessionManager> = this.language$.pipe(
    distinctUntilChanged(),
    switchMap((lang) => from(this.speech.ensureSession(lang))),
    shareReplay(1)
  );

  readonly loadingSession$: Observable<boolean> = this.session$.pipe(
    switchMap(s => s.state$.pipe(startWith(s.state))),
    map(s => s === AsrSessionState.StartingSession),
    startWith(true)
  );

  /**
   * Create a GrammarInteraction instance for the provided exercise
   * and session.
   */
  readonly interactionSet$: Observable<NovoSpeechGrammarInteraction> = combineLatest([this.exercise$, this.session$]).pipe(
    switchMap(([exercise, session]) => {
      this.error$.next(null);
      return from(this.speech.getMultipleChoiceGrammar(session, exercise.options)).pipe(
        catchError(err => {
          this.error$.next(typeof err === 'object' && err.message ? err.message : err);
          console.error(err);
          return EMPTY;
        }),
      );
    }),
    shareReplay(1),
  );

  /**
   * Read intermediate results from the active interaction set
   */
  readonly intermediateResults$: Observable<AsrIntermediateResult> = this.interactionSet$.pipe(
    switchMap(i => i.intermediateResults$),
    filter((r): r is AsrIntermediateResult => isIntermediateAsrResult(r)),
  );

  /**
   * Read final results from the interaction set
   */
  readonly asrResult$: Observable<AsrResult> = this.interactionSet$.pipe(
    switchMap(i => i.result$),
    filter((r): r is AsrResult => !isIntermediateAsrResult(r)),
  );

  /**
   * Expose AsrGrammarInteractionState enum to template
   */
  readonly AsrGrammarInteractionState: typeof AsrGrammarInteractionState = AsrGrammarInteractionState;

  @Input() responseOptions!: string;

  constructor(readonly speech: NovoSpeechController) { }

  ngOnInit() {
    this.language$.next('en');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.responseOptions?.currentValue) {
      const options = this.responseOptions.split(/[\r\n]+/);
      this.exercise$.next({ options });
    }
  }

  async toggleRecording(interactionSet: NovoSpeechGrammarInteraction) {
    try {
      if (!interactionSet.isActivated()) {
        await interactionSet.activate();
      }
      (interactionSet.isRecording()) ? interactionSet.stop() : interactionSet.record();
    } catch (err) {
      console.error(`Can't start recording`, err?.message || err);
    }
  }

  // languageChange(event: Event) {
  //   const lang = event.target!.value!;
  //   this.language$.next(lang);
  // }

}
