import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  responseOptions: string = ['this is a test', 'hello', 'option'].join('\n');
  activeResponseOptions: string;

  setExercise() {
    this.activeResponseOptions = this.responseOptions;
  }
}
